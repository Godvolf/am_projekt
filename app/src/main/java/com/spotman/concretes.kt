package com.spotman

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.util.ArrayMap
import android.support.v4.util.ArraySet
import android.support.v4.view.PagerAdapter
import android.widget.Toast
import com.spotman.ExportSongsModule.ExportSongsFragment
import com.spotman.browser.*
import com.spotman.data.Playlist
import com.spotman.data.SongUri
import com.spotman.filter.FiltersAdapter
import com.spotman.filter.FiltersFragment
import com.spotman.filter.PreferencePersistentFiltersModel
import com.spotman.push.*
import com.spotman.service.CallbackData
import com.spotman.sources.MainSearchFragment
import com.spotman.templates.*
import java.text.SimpleDateFormat
import java.util.*

class ConcreteAuthActivity : AuthActivity() {

    override val someBrowserActivity: Class<*> = ConcreteBrowserActivity::class.java
}

class ConcreteBrowserActivity : BrowserActivity<DataSongMeta>() {

    override val pager: PagerAdapter = ActionsPagerAdapter()
    override val model: PreferencePersistentSongsModel<DataSongMeta> =
        ConcreteSongsModel(DataSongMetaFromPreferencesFactory())
    override val adapter: BrowserAdapter = BrowserAdapter(model, this)

    val destinations = ConcreteDestinationsFacadeProxy()
    val playlistSearch = ConcretePlaylistSearchFacadeProxy()

    override val callback: TokenReceivedCallback = object: TokenReceivedCallback {

        override fun onTokenReceived(token: String) {
            destinations.activate(token)
            playlistSearch.activate(token)
        }
    }

    inner class ActionsPagerAdapter : FragmentStatePagerAdapter(supportFragmentManager) {

        override fun getItem(idx: Int): Fragment = when (idx) {
            0 -> {
                val fragment = MainSearchFragment()
                fragment.callback.wrapped = model
                fragment
            }
            1 -> {
                val fragment = ConcreteTemplatesFragment()
                fragment.templates.activate(this@ConcreteBrowserActivity)
                fragment.songs = model
                fragment
            }
/*            2 -> {
                val fragment = ConcreteFiltersFragment()
                fragment.sharedPreferences = { getSharedPreferences(FILTERS_PREFERENCES_KEY, Context.MODE_PRIVATE)}
                fragment
            }*/
             2 -> {
                 val fragment = ConcretePushFragment()
                 fragment.songs = model
                 fragment.destinations = destinations
                 fragment.playlists = playlistSearch
                 fragment
             }
            else -> {
                val fragment = ExportSongsFragment()
                fragment.model = model
                fragment
            }
        }

        override fun getCount(): Int = 4

        override fun getPageTitle(idx: Int): CharSequence? = when (idx) {
            0 -> "Pull"
            1 -> "Template"
//            2 -> "Filter"
            2 -> "Push"
            else -> "Export"
        }
    }

    inner class ConcretePlaylistSearchFacadeProxy: PlaylistSearchFacadeProxy() {
        override var wrapped: AbstractOwnPlaylistFacade = object : AbstractOwnPlaylistFacade {

            override fun fetchAllOwnPlaylists(callback: CallbackData<List<Playlist>>) {
                Toast.makeText(this@ConcreteBrowserActivity, R.string.browser_toast_authneeded, Toast.LENGTH_SHORT).show()
            }
        }
    }

    inner class ConcreteDestinationsFacadeProxy: DestinationsFacadeProxy() {
        override var wrapped: AbstractDestinationsFacade = object : AbstractDestinationsFacade {
            override fun createNewPlaylist(
                name: String, description: String, songUriList: List<SongUri>, callback: CallbackData<String>
            ) {
                Toast.makeText(this@ConcreteBrowserActivity, R.string.browser_toast_authneeded, Toast.LENGTH_SHORT).show()
            }

            override fun addSongsToPlaylist(
                playlistId: String, songUriList: List<SongUri>, callback: CallbackData<String>
            ) {
                Toast.makeText(this@ConcreteBrowserActivity, R.string.browser_toast_authneeded, Toast.LENGTH_SHORT).show()
            }

            override fun removeMatchingSongsFromPlaylist(
                playlistId: String, songUriList: List<SongUri>, callback: CallbackData<String>
            ) {
                Toast.makeText(this@ConcreteBrowserActivity, R.string.browser_toast_authneeded, Toast.LENGTH_SHORT).show()
            }
        }
    }
}

class ConcreteTemplatesFragment: TemplatesFragment<DataSongMeta>() {

    override val templates = TemplatesModelProxy()
    override val adapter: TemplatesAdapter = TemplatesAdapter(templates, this)
    override var songs: SetlikeAggregator<DataSongMeta>? = null
}

class ConcreteFiltersFragment : FiltersFragment() {

    override val model: PreferencePersistentFiltersModel = ConcreteFiltersModel()
    override val adapter = FiltersAdapter(model, this)

}

class ConcretePushFragment: PushFragment() {
    override val model: PushModel = ConcretePushModel()
    override val adapter: PushAdapter = PushAdapter(model, this)
}

class MockTemplatesModel : TemplatesModel<DataSongMeta> {

    private val list = ArrayList<TemplateMeta>()
    private val map = ArrayMap<TemplateMeta, Iterable<DataSongMeta>>()
    private val observers = ArraySet<ListObserver<TemplateMeta>>()

    init {
        list.add(DataTemplateMeta("20190504T1215+0200", "a/tormessuinex_silichordium"))
        list.add(DataTemplateMeta("20190208T1610+0200", "a/septentus_concordiam"))
    }

    override fun createTemplate(name: String, time: Date, elems: Iterable<DataSongMeta>) {
        val old = ArrayList(list)
        val template = DataTemplateMeta(SimpleDateFormat(TEMPLATES_DATEFORMAT).format(time), name)
        list.add(template)
        map[template] = elems
        observers.forEach { it.onDataSetChanged(old, list) }
    }

    override fun length(): Int = list.size

    override fun get(idx: Int): TemplateMeta = list[idx]

    override fun removeAt(idx: Int) {
        val old = ArrayList(list)
        list.removeAt(idx)
        observers.forEach { it.onDataSetChanged(old, list) }
    }

    override fun register(observer: ListObserver<TemplateMeta>) {
        observers.add(observer)
    }

    override fun unregister(observer: ListObserver<TemplateMeta>) {
        observers.remove(observer)
    }

    override fun fetchAllOf(idx: Int): Iterable<DataSongMeta> {
        val template = list[idx]
        val templateSong = DataSongMeta(
            template.name().toString(),
            "user",
            template.ctime(TEMPLATES_DATEFORMAT).toString(),
            "spotify:track:6ZFbXIJkuI1dVNWvzJzown"
        )
        return map[template] ?: return Collections.singleton(templateSong)
    }
}