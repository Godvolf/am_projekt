package com.spotman

import com.spotman.data.Playlist
import com.spotman.facade.SourcesFacade
import com.spotman.push.AbstractOwnPlaylistFacade
import com.spotman.service.CallbackData

abstract class PlaylistSearchFacadeProxy : AbstractOwnPlaylistFacade {

    abstract var wrapped: AbstractOwnPlaylistFacade

    fun activate(token: String) {
        wrapped = SourcesFacade(token)
    }

    override fun fetchAllOwnPlaylists(callback: CallbackData<List<Playlist>>) {
        wrapped.fetchAllOwnPlaylists(callback)
    }
}
