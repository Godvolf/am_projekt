package com.spotman

/**
 * Client controls accessing elements within bounds based on length().
 */
interface ReadableList<out Type> {
    fun length(): Int
    fun get(idx: Int): Type
}
