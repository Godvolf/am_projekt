package com.spotman.templates

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.spotman.R
import com.spotman.SetlikeAggregator
import kotlinx.android.synthetic.main.templates.*
import java.util.*

const val TEMPLATES_DATEFORMAT = "yyyy-MM-dd HH:mm:ssZZZZ"

abstract class TemplatesFragment<Aggregatee>: Fragment(), TemplatesAdapterCallback, TemplateCreationCallback {

    abstract val templates: TemplatesModel<Aggregatee>
    abstract var songs: SetlikeAggregator<Aggregatee>?
    abstract val adapter: TemplatesAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.templates, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        templates_create.setOnClickListener {
            val dialog = TemplateCreationDialog()
            dialog.callback = this
            dialog.show(fragmentManager, TEMPLATES_CREATION_TAG)
        }

        val callback = ItemTouchHelperTemplatesCallback(templates)

        templates_recycler.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        templates_recycler.setHasFixedSize(true)
        templates_recycler.adapter = adapter
        ItemTouchHelper(callback).attachToRecyclerView(templates_recycler)

        templates.register(adapter)
    }

    override fun onDestroyView() {
        templates.unregister(adapter)
        super.onDestroyView()
    }

    override fun loadTracksOf(idx: Int) {
        val elems = templates.fetchAllOf(idx)
        songs?.clear()
        songs?.addUnique(elems)
    }

    override fun supplyTracksOf(idx: Int) {
        val elems = templates.fetchAllOf(idx)
        songs?.addUnique(elems)
    }

    override fun subtractTracksOf(idx: Int) {
        val elems = templates.fetchAllOf(idx)
        songs?.removeMatching(elems)
    }

    override fun onTemplateMetaReceived(name: String, time: Date) {
        val elems = songs?.fetchAll()
        if(elems != null)
            templates.createTemplate(name, time, elems)
    }
}
