package com.spotman.templates

data class DataTemplateMeta(private val ctime: CharSequence, private val name: CharSequence) : TemplateMeta {
    override fun ctime(dateformat: CharSequence): CharSequence = ctime

    override fun name(): CharSequence = name
}