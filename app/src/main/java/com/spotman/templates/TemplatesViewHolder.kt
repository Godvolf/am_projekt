package com.spotman.templates

import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.templates_vh.view.*

class TemplatesViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    val ctime: AppCompatTextView = itemView.templates_ctime
    val name: AppCompatTextView = itemView.templates_name
    val supply: AppCompatImageView = itemView.templates_supply
    val subtract: AppCompatImageView = itemView.templates_subtract
}
