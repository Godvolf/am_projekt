package com.spotman.templates

import java.util.*

interface TemplateCreationCallback {
    fun onTemplateMetaReceived(name: String, time: Date)
}
