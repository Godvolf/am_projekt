package com.spotman.templates

interface TemplatesAdapterCallback {
    fun loadTracksOf(idx: Int)
    fun supplyTracksOf(idx: Int)
    fun subtractTracksOf(idx: Int)

}
