package com.spotman.templates

import com.spotman.ObservableList
import com.spotman.ReadableList
import com.spotman.RemovableList
import java.util.*

interface TemplatesModel<Aggregatee> : ReadableList<TemplateMeta>, RemovableList, ObservableList<TemplateMeta> {
    fun fetchAllOf(idx: Int): Iterable<Aggregatee>
    fun createTemplate(name: String, time: Date, elems: Iterable<Aggregatee>)
}
