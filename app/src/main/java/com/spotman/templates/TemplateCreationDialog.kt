package com.spotman.templates

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import com.spotman.R
import kotlinx.android.synthetic.main.templates_creation.view.*
import java.util.*

const val TEMPLATES_CREATION_TAG = "TEMPLATES_CREATION_TAG"

class TemplateCreationDialog : DialogFragment(), TemplateCreationCallback {

    var callback: TemplateCreationCallback = this

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val builder = AlertDialog.Builder(context)

        builder.setTitle(R.string.templates_creation_title)
        val view = LayoutInflater.from(context).inflate(R.layout.templates_creation, null, false)
        val editText = view.templates_creation_input
        builder.setView(view)

        builder.setNegativeButton(android.R.string.cancel) { dialog, _ ->
            dialog.dismiss()
        }
        builder.setPositiveButton(android.R.string.ok) { _, _ ->
            callback.onTemplateMetaReceived(editText.text.toString(), Calendar.getInstance().time)
        }

        return builder.create()
    }

    override fun onTemplateMetaReceived(name: String, time: Date) {}
}
