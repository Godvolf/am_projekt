package com.spotman.templates

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import com.spotman.RemovableList

class ItemTouchHelperTemplatesCallback(private val model: RemovableList) :
    ItemTouchHelper.SimpleCallback(0, 4 or 8) {

    override fun onMove(p0: RecyclerView, p1: RecyclerView.ViewHolder, p2: RecyclerView.ViewHolder): Boolean {
        return false
    }

    override fun onSwiped(vh: RecyclerView.ViewHolder, dir: Int) {
        val idx = vh.adapterPosition
        model.removeAt(idx)
    }

}
