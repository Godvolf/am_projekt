package com.spotman.templates

interface TemplateMeta {
    fun ctime(dateformat: CharSequence): CharSequence
    fun name(): CharSequence
}
