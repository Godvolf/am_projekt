package com.spotman.templates

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.spotman.ListObserver
import com.spotman.R
import com.spotman.ReadableList

class TemplatesAdapter(private val model: ReadableList<TemplateMeta>, private val callback: TemplatesAdapterCallback) :
    RecyclerView.Adapter<TemplatesViewHolder>(), ListObserver<TemplateMeta> {

    override fun onCreateViewHolder(group: ViewGroup, idx: Int): TemplatesViewHolder {
        val view = LayoutInflater.from(group.context).inflate(R.layout.templates_vh, group, false)
        val vh = TemplatesViewHolder(view)

        vh.itemView.setOnClickListener {
            callback.loadTracksOf(vh.adapterPosition)
        }
        vh.supply.setOnClickListener {
            callback.supplyTracksOf(vh.adapterPosition)
        }
        vh.subtract.setOnClickListener {
            callback.subtractTracksOf(vh.adapterPosition)
        }
        return vh
    }

    override fun getItemCount(): Int = model.length()

    override fun onBindViewHolder(vh: TemplatesViewHolder, idx: Int) {
        val template = model.get(idx)
        vh.ctime.text = template.ctime(TEMPLATES_DATEFORMAT)
        vh.name.text = template.name()
    }

    override fun onDataSetChanged(old: List<TemplateMeta>, new: List<TemplateMeta>) {

        val callback = object : DiffUtil.Callback() {

            override fun areItemsTheSame(oIdx: Int, nIdx: Int): Boolean {
                val a = old[oIdx]
                val b = new[nIdx]
                return a.name() == b.name() && a.ctime(TEMPLATES_DATEFORMAT) == b.ctime(TEMPLATES_DATEFORMAT)
            }

            override fun getOldListSize(): Int = old.size

            override fun getNewListSize(): Int = new.size

            override fun areContentsTheSame(oIdx: Int, nIdx: Int): Boolean {
                val a = old[oIdx]
                val b = new[nIdx]
                return a.name() == b.name() && a.ctime(TEMPLATES_DATEFORMAT) == b.ctime(TEMPLATES_DATEFORMAT)
            }
        }

        val result = DiffUtil.calculateDiff(callback)
        result.dispatchUpdatesTo(this)
    }
}
