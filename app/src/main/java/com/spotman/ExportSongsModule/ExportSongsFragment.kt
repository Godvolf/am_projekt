package com.spotman.ExportSongsModule

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.spotman.ElementProvider
import com.spotman.R
import com.spotman.SongMeta
import kotlinx.android.synthetic.main.fragment_export_songs.*


class ExportSongsFragment : Fragment() {

    private val exporter = ExportSongsModule()
    var model: ElementProvider<SongMeta>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_export_songs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        export_songs_button.setOnClickListener {
            model?.fetchAll()?.forEach {
                exporter.addToList(
                    Song(
                        it.title().toString(),
                        it.artist().toString(),
                        it.album().toString(),
                        0
                    )
                )
            }
            if(hasExportPermission()) {
                val path = exporter.writeListToFileCSV()
                Toast.makeText(context, "Song list exported to: $path", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(context, "Save error: Insufficient privileges!", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun hasExportPermission() : Boolean {
        if (ContextCompat.checkSelfPermission(context!!, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED
            || ContextCompat.checkSelfPermission(context!!, Manifest.permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED) {
            return false
        }
        return true
    }
}
