package com.spotman.ExportSongsModule

import android.os.Environment
import com.opencsv.CSVWriter
import java.io.File
import java.io.FileWriter
import java.util.UUID

class ExportSongsModule {

    var list = ArrayList<Song>()

    fun clearList() {
        list.clear()
    }

    fun addToList(song: Song) {
        list.add(song)
    }

    fun removeFromList(song: Song) {
        list.remove(song)
    }

    fun assignList(songs: ArrayList<Song>) {
        list = songs
    }

    fun writeListToFileCSV() : String {
        val uuid = UUID.randomUUID()
        val filename = "$uuid.csv"
        val directory = Environment.getExternalStorageDirectory().toString() + "/spotman/"
        val spotmanDirectory = File(directory)

        if(!spotmanDirectory.exists()) {
            spotmanDirectory.mkdirs()
        }

        val fullPath = directory + filename

        val writer = CSVWriter(
            FileWriter(fullPath),
            CSVWriter.DEFAULT_SEPARATOR,
            CSVWriter.NO_QUOTE_CHARACTER,
            CSVWriter.DEFAULT_ESCAPE_CHARACTER,
            CSVWriter.DEFAULT_LINE_END
        )

        writer.writeNext(
            Song().toString()
                .removePrefix("Song(")
                .removeSuffix("=)")
                .removeSuffix("=0)")
                .split("=[A-z0-9]*, ".toRegex())
                .toTypedArray()
        )

        val temp: MutableList<Array<String>> = ArrayList()

        list.forEach {
            temp.add(
                it.toString()
                    .removePrefix("Song(")
                    .removeSuffix(")")
                    .split(", ".toRegex())
                    .toTypedArray()
            )
        }

        writer.writeAll(temp)
        writer.toString()
        writer.close()

        return fullPath
    }
}