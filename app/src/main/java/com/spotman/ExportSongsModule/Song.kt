package com.spotman.ExportSongsModule

data class Song(
    var title: String = "",
    var author: String = "",
    var descryption:  String = "",
    var length: Int = 0

) {
    constructor(temp: ArrayList<List<String>>) : this(){

        temp.forEach {
            when(it[0]){
                "title" -> this.title = it[1]
                "author" -> this.author = it[1]
                "decryption" -> this.descryption = it[1]
                "length" -> this.length = it[1].toInt()


            }

        }
    }
}