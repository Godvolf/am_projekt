package com.spotman.ExportSongsModule

import android.os.Environment
import android.util.Log
import com.opencsv.CSVReader
import com.opencsv.CSVWriter
import org.apache.commons.lang3.mutable.Mutable
import java.io.BufferedReader
import java.io.FileReader
import java.io.FileWriter


class ImportSongsModule {


    fun importList(fileNamee: String = "csvSomething.csv" ) : ArrayList<Song>{
        var fileName =  Environment.getExternalStorageDirectory().toString() + "/" + fileNamee

        var csvReader = CSVReader(FileReader(fileName))

        var list = ArrayList<Song>()

        var record : Array<String>?

        csvReader.readNext() // skip Header


        record = csvReader.readNext()
        var temp : List<String>
        while (record != null) {
            var temp :ArrayList<List<String>> = arrayListOf<List<String>>()
            record.forEach {
                temp.add(it.split("="))
            }

            list.add(Song(temp))
            Log.d("WRITE READ",record[0] + " | " + record[1] + " | " + record[2] + " | " + record[3])
            record = csvReader.readNext()
        }

        csvReader.close()

        return list


    }


}