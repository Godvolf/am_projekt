package com.spotman.data

class Album {
    var id : String
    var name : String
    var uri : String
    var images : List<Image>
    var artists : List<Artist>
    var releaseDate : String



    private constructor(builder: Builder){
        this.id = builder.id
        this.images = builder.images
        this.name = builder.name
        this.artists = builder.artists
        this.releaseDate = builder.releaseDate
        this.uri = builder.uri
    }

    companion object {
        fun builder() : Builder{
            return Builder()
        }
    }

    class Builder {

        var id : String = ""
        var images : List<Image> = ArrayList()
        var name : String = ""
        var uri : String = ""
        var artists : List<Artist> = ArrayList()
        var releaseDate : String = "1900-01-01"

        fun withId(id : String) : Builder{
            this.id = id
            return this
        }

        fun withImages(images : List<Image>) : Builder {
            this.images = images
            return this
        }

        fun withName(name : String) : Builder{
            this.name = name
            return this
        }

        fun withArtists(artists : List<Artist>) : Builder{
            this.artists = artists
            return this
        }

        fun withReleaseDate(releaseDate : String ) : Builder{
            this.releaseDate = releaseDate
            return this
        }

        fun withUri(uri : String) : Builder{
            this.uri = uri
            return this
        }

        fun build() : Album {
            return Album(this)
        }
    }
}
