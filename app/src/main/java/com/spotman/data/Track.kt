package com.spotman.data

class Track : SongUri{

    var id : String
    var name : String
    var uri : String
    var durationMs : Int
    var popularity : Int
    var artists : List<Artist>
    var album : Album

    private constructor(builder: Builder){
        this.id = builder.id
        this.name = builder.name
        this.artists = builder.artists
        this.uri = builder.uri
        this.durationMs = builder.durationMs
        this.popularity = builder.popularity
        this.album = builder.album
    }

    companion object {
        fun builder() : Builder{
            return Builder()
        }
    }

    override fun getSongUri(): String {
        return uri;
    }

    class Builder {

        var id : String = ""
        var name : String = ""
        var uri : String = ""
        var durationMs : Int = 0
        var popularity : Int = 0
        var artists : List<Artist> = ArrayList()
        var album : Album = Album.builder().build()

        fun withId(id : String) : Builder{
            this.id = id
            return this
        }

        fun withName(name : String) : Builder{
            this.name = name
            return this
        }

        fun withUri(uri : String) : Builder{
            this.uri = uri
            return this
        }

        fun withDurationMs(durationMs : Int) : Builder{
            this.durationMs = durationMs
            return this
        }

        fun withPopularity(popularity : Int) : Builder{
            this.popularity = popularity
            return this
        }


        fun withArtists(artists : List<Artist>) : Builder{
            this.artists = artists
            return this
        }

        fun withAlbum(album : Album) : Builder{
            this.album = album
            return this
        }

        fun build() : Track {
            return Track(this)
        }
    }

}
