package com.spotman.data

interface SongUri {
    fun getSongUri() : String
}