package com.spotman.data

import com.spotman.push.PlaylistMeta

class Playlist: PlaylistMeta {

    var id : String
    var images : List<Image>
    var name : String
    var owner : User
    var public : Boolean
    var uri : String

    private constructor(builder: Builder){
        this.id = builder.id
        this.images = builder.images
        this.name = builder.name
        this.owner = builder.owner
        this.public = builder.public
        this.uri = builder.uri
    }

    companion object {
        fun builder() : Builder{
            return Builder()
        }
    }

    class Builder {

        var id : String = ""
        var images : List<Image> = ArrayList<Image>()
        var name : String = ""
        var owner : User = User("Empty", "Empty", "")
        var public : Boolean = false
        var uri : String = ""

        fun withId(id : String) : Builder{
            this.id = id
            return this
        }

        fun withImages(images : List<Image>) : Builder{
            this.images = images
            return this
        }

        fun withName(name : String) : Builder{
            this.name = name
            return this
        }

        fun withOwner(owner : User) : Builder{
            this.owner = owner
            return this
        }

        fun withPublic(public : Boolean) : Builder{
            this.public = public
            return this
        }

        fun withUri(uri : String) : Builder{
            this.uri = uri
            return this
        }

        fun build() : Playlist {
            return Playlist(this)
        }
    }

    override fun title(): CharSequence = name

    override fun playlistId(): CharSequence = id
}