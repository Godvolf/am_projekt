package com.spotman

import android.content.SharedPreferences
import android.support.v4.util.ArraySet
import com.spotman.filter.FILTERS_PREFERENCES_IDS_KEY
import com.spotman.filter.FilterMeta
import com.spotman.filter.PreferencePersistentFiltersModel

class ConcreteFiltersModel : PreferencePersistentFiltersModel {

    private val factory: FromPreferenceFactory<DataFilterMeta> = DataFilterMeta.FromPreferenceFactory()
    private val list = ArrayList<DataFilterMeta>()
    private val observers = ArraySet<ListObserver<DataFilterMeta>>()

    override fun createFilter(attribute: CharSequence, value: CharSequence) {
        val old = ArrayList(list)
        list.add(DataFilterMeta(attribute.toString(), value.toString()))
        observers.forEach { it.onDataSetChanged(old, list) }
    }

    override fun updateFilterAt(idx: Int, attribute: CharSequence, value: CharSequence) {
        val old = ArrayList(list)
        list[idx] = DataFilterMeta(attribute.toString(), value.toString())
        observers.forEach { it.onDataSetChanged(old, list) }
    }

    override fun toggleFilterAt(idx: Int) {
        val old = ArrayList(list)
        list[idx] = DataFilterMeta(list[idx].attribute, list[idx].value, !list[idx].toggled)
        observers.forEach { it.onDataSetChanged(old, list) }
    }

    override fun removeAt(idx: Int) {
        val old = ArrayList(list)
        list.removeAt(idx)
        observers.forEach { it.onDataSetChanged(old, list) }
    }

    override fun register(observer: ListObserver<FilterMeta>) {
        observers.add(observer)
    }

    override fun unregister(observer: ListObserver<FilterMeta>) {
        observers.remove(observer)
    }

    override fun length(): Int = list.size

    override fun get(idx: Int): DataFilterMeta = list[idx]

    override fun save(preferences: SharedPreferences) {
        preferences.edit().clear().commit()
        list.forEach { it.save(preferences) }
    }

    override fun restore(preferences: SharedPreferences) {
        val old = ArrayList(list)
        list.clear()
        preferences.getStringSet(FILTERS_PREFERENCES_IDS_KEY, emptySet())?.forEach {
            list.add(factory.restoreFrom(it, preferences))
        }
        observers.forEach { it.onDataSetChanged(old, list) }
    }
}