package com.spotman.dto

data class NewPlaylistRequest (
    var name : String,
    var description : String
){
}