package com.spotman.dto.album

import com.spotman.dto.artists.ArtistsDTO
import com.spotman.dto.ImageDTO

data class AlbumDTO(
    var href : String,
    var artists : List<ArtistsDTO>,
    var images : List<ImageDTO>,
    var id : String,
    var name : String,
    var type : String,
    var uri : String,
    var album_type : String,
    var release_date : String
) {
}
