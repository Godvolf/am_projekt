package com.spotman.dto.album

import com.spotman.dto.artists.ArtistsDTO
import com.spotman.dto.ImageDTO
import com.spotman.dto.song.SongDTO


data class AlbumResponse(
    var href : String,
    var artists : List<ArtistsDTO>,
    var images : List<ImageDTO>,
    var id : String,
    var name : String,
    var type : String,
    var uri : String,
    var album_type : String,
    var release_date : String,
    var tracks : TrackAlbumResponse
) {

    fun getAlbumDTO() : AlbumDTO {
        return AlbumDTO(href, artists, images, id, name, type, uri, album_type, release_date)
    }
}

data class TrackAlbumResponse(
    var href : String,
    var items : List<SongDTO>,
    var limit : Int,
    var offset : Int,
    var total : Int
){
}