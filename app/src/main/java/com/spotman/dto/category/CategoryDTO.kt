package com.spotman.dto.category

import com.spotman.dto.ImageDTO

data class CategoryDTO(
    var href : String,
    val icons : List<ImageDTO>,
    var id : String,
    var name : String) {
}
