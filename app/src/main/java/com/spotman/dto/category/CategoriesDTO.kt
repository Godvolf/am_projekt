package com.spotman.dto.category

data class CategoriesDTO(
    var href : String,
    val items : List<CategoryDTO>,
    var limit : Int,
    var next : String,
    var offset : Int,
    var previous : String,
    var total : Int) {
}
