package com.spotman.dto.song

data class TrackResponse(
    var href : String,
    var items : List<TrackDTO>,
    var limit : Int,
    var offset : Int,
    var total : Int
) {
}
