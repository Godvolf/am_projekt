package com.spotman.dto.song

import com.spotman.dto.song.SongDTO

class TrackDTO(
    var added_at : String,
    var is_local : Boolean,
    var track : SongDTO
) {
}

