package com.spotman.dto.song

import com.spotman.dto.artists.ArtistsDTO
import com.spotman.dto.album.AlbumDTO

data class SongDTO(
    var artists : List<ArtistsDTO>,
    var album : AlbumDTO,
    var href : String,
    var id : String,
    var name : String,
    var type : String,
    var uri : String,
    var duration_ms : Int,
    var popularity : Int
) {
}
