package com.spotman.dto.artists

data class ArtistsDTO(
    var href : String,
    var id : String,
    var name : String,
    var type : String,
    var uri : String
) {
}

