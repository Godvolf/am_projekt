package com.spotman.dto.playlist

import com.spotman.dto.ImageDTO
import com.spotman.dto.UserDTO

data class PlaylistDTO(
    var href : String,
    var id : String,
    var images : List<ImageDTO>,
    var collaborative : Boolean,
    var name : String,
    var owner : UserDTO,
    var public : Boolean,
    var type : String,
    var uri : String
) {
}
