package com.spotman.dto.playlist

class PlaylistResponse(
    var href : String,
    var items : List<PlaylistDTO>
) {
}