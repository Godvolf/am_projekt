package com.spotman.dto

class ImageDTO(
    var height : Int,
    var width : Int,
    var url : String)  {
}
