package com.spotman.dto.search

data class SearchResponse<T>(
    var href : String,
    var items : List<T>,
    var limit : Int,
    var total : Int
){
}
