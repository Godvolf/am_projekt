package com.spotman.dto.search

import com.spotman.dto.album.AlbumDTO
import com.spotman.dto.artists.ArtistsDTO
import com.spotman.dto.playlist.PlaylistDTO
import com.spotman.dto.song.SongDTO

data class SearchAllResponse (
    var albums : SearchResponse<AlbumDTO>,
    var artists : SearchResponse<ArtistsDTO>,
    var playlists : SearchResponse<PlaylistDTO>,
    var tracks : SearchResponse<SongDTO>
) {
}