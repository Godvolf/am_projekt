package com.spotman

import android.content.SharedPreferences

interface PreferencePersistent {
    fun save(preferences: SharedPreferences)
}
