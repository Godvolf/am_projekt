package com.spotman

import android.content.SharedPreferences
import android.support.v4.util.ArraySet
import android.util.Log
import com.spotman.browser.BROWSER_PREFERENCES_SONG_IDS_KEY
import com.spotman.browser.DataSongMeta
import com.spotman.browser.PreferencePersistentSongsModel

class ConcreteSongsModel<Aggregatee : PreferencePersistent>(private val factory: FromPreferenceFactory<Aggregatee>) :
    PreferencePersistentSongsModel<Aggregatee> {

    private val list = ArrayList<Aggregatee>()
    private val observers = ArraySet<ListObserver<Aggregatee>>()

    /*init {
        list.add(DataSongMeta("Time", "Hans Zimmer", "Inception", "spotify:track:6ZFbXIJkuI1dVNWvzJzown"))
        list.add(DataSongMeta("LIS", "Dawid Podsiadło", "Malomiasteczkowy", "spotify:track:6MBvwe7DJPyMnmwXX86mKX"))
        list.add(DataSongMeta("Levels - Radio Edit", "Avicii", "Levels", "spotify:track:5UqCQaDshqbIk3pkhy4Pjg"))
        list.add(DataSongMeta("Hurt", "Nine Inch Nails", "The Downward Spiral", "spotify:track:27tX58NOpv1YKQ0abW7EPy"))
        list.add(DataSongMeta("Clash", "Caravan Palace", "Panic", "spotify:track:4J1hExlfMoDhGsCMIiY3Zs"))
        list.add(DataSongMeta("Fire", "Barns Courtney", "The Attractions Of Youth", "spotify:track:5w6B0sAH7XauCvMOAtplQj"))
    }*/

    override fun removeAt(idx: Int) {
        val old = ArrayList(list)
        list.removeAt(idx)
        observers.forEach { it.onDataSetChanged(old, list) }
    }

    override fun register(observer: ListObserver<Aggregatee>) {
        observers.add(observer)
    }

    override fun unregister(observer: ListObserver<Aggregatee>) {
        observers.remove(observer)
    }

    override fun clear() {
        val old = ArrayList(list)
        list.clear()
        observers.forEach { it.onDataSetChanged(old, list) }
    }

    override fun length(): Int = list.size

    override fun get(idx: Int): Aggregatee = list[idx]

    override fun addUnique(elements: Iterable<Aggregatee>) {
        val old = ArrayList(list)
        list.addAll(elements.filter { !list.contains(it) })
        observers.forEach { it.onDataSetChanged(old, list) }
    }

    override fun removeMatching(elements: Iterable<Aggregatee>) {
        val old = ArrayList(list)
        list.removeAll(elements)
        observers.forEach { it.onDataSetChanged(old, list) }
    }

    override fun fetchAll(): Iterable<Aggregatee> = list // TODO nie naruszy mutability?

    override fun save(preferences: SharedPreferences) {
        preferences.edit().clear().commit()
        list.forEach { it.save(preferences) }
    }

    override fun restore(preferences: SharedPreferences) {
        val old = ArrayList(list)
        list.clear()
        val keys = preferences.getStringSet(BROWSER_PREFERENCES_SONG_IDS_KEY, emptySet()) ?: emptySet<String>()
        keys.forEach {
            list.add(factory.restoreFrom(it, preferences))
        }
        observers.forEach { it.onDataSetChanged(old, list) }
    }

    override fun onTracksReceived(tracks: Iterable<Aggregatee>) {
        addUnique(tracks)
    }
}
