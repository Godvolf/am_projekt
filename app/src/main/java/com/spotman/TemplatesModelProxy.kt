package com.spotman

import android.content.Context
import com.spotman.browser.DataSongMeta
import com.spotman.databaseModule.DatabaseMenager
import com.spotman.templates.DataTemplateMeta
import com.spotman.templates.TemplateMeta
import com.spotman.templates.TemplatesModel
import java.util.*

class TemplatesModelProxy : TemplatesModel<DataSongMeta> {

    private var wrapped: TemplatesModel<DataSongMeta> = NullTemplatesModel()

    fun activate(context: Context) {
        wrapped = DatabaseMenager(context)
    }

    override fun fetchAllOf(idx: Int): Iterable<DataSongMeta> = wrapped.fetchAllOf(idx)

    override fun createTemplate(name: String, time: Date, elems: Iterable<DataSongMeta>) {
        wrapped.createTemplate(name, time, elems)
    }

    override fun length(): Int = wrapped.length()

    override fun get(idx: Int): TemplateMeta = wrapped.get(idx)

    override fun removeAt(idx: Int) {
        wrapped.removeAt(idx)
    }

    override fun register(observer: ListObserver<TemplateMeta>) {
        wrapped.register(observer)
    }

    override fun unregister(observer: ListObserver<TemplateMeta>) {
        wrapped.unregister(observer)
    }

    class NullTemplatesModel: TemplatesModel<DataSongMeta> {
        override fun fetchAllOf(idx: Int): Iterable<DataSongMeta> = emptyList()

        override fun createTemplate(name: String, time: Date, elems: Iterable<DataSongMeta>) {}

        override fun length(): Int = 0

        override fun get(idx: Int): TemplateMeta = DataTemplateMeta("null", "null")

        override fun removeAt(idx: Int) {}

        override fun register(observer: ListObserver<TemplateMeta>) {}

        override fun unregister(observer: ListObserver<TemplateMeta>) {}
    }
}
