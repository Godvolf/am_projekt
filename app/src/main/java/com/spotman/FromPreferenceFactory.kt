package com.spotman

import android.content.SharedPreferences

interface FromPreferenceFactory<out Aggregatee> {
    fun restoreFrom(key: String, preferences: SharedPreferences): Aggregatee
}
