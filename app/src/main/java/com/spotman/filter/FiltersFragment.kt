package com.spotman.filter

import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.spotman.R
import kotlinx.android.synthetic.main.filters.*

const val FILTERS_PREFERENCES_IDS_KEY = "FILTERS_PREFERENCES_IDS_KEY"
const val FILTERS_PREFERENCES_KEY = "FILTERS_PREFERENCES_KEY"

abstract class FiltersFragment : Fragment(), FiltersAdapterCallback {

    abstract val model: PreferencePersistentFiltersModel
    abstract val adapter: FiltersAdapter
    var sharedPreferences: () -> SharedPreferences? = { null }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.filters, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val prefs = sharedPreferences()
        if(prefs != null) model.restore(prefs)

        filters_recycler.adapter = adapter
        filters_recycler.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        filters_recycler.setHasFixedSize(true)
        ItemTouchHelper(FiltersItemTouchHelperCallback(model)).attachToRecyclerView(filters_recycler)

        filters_create.setOnClickListener {
            val dialog = FilterCreationDialog()
            dialog.onFilterCreationFinishedListener = {attribute, value ->
                model.createFilter(attribute, value)
            }
            dialog.show(fragmentManager, FILTERS_CREATION_TAG)
        }

        model.register(adapter)
    }

    override fun onDestroyView() {
        val prefs = sharedPreferences()
        if(prefs != null)
            model.save(prefs)
        model.unregister(adapter)
        super.onDestroyView()
    }

    override fun editFilterAt(idx: Int) {
        val dialog = FilterCreationDialog()
        val filter = model.get(idx)
        val args = Bundle(2)
        args.putString(FILTERS_CREATION_ATTRIBUTE_KEY, filter.attrKey().toString())
        args.putString(FILTERS_CREATION_VALUE_KEY, filter.attrVal().toString())
        dialog.arguments = args
        dialog.onFilterCreationFinishedListener = { attribute, value ->
            model.updateFilterAt(idx, attribute, value)
        }
        dialog.show(fragmentManager, FILTERS_CREATION_TAG)
    }

    override fun toggleFilterAt(idx: Int) {
        model.toggleFilterAt(idx)
    }
}