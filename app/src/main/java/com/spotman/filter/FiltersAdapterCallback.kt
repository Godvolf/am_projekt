package com.spotman.filter

interface FiltersAdapterCallback {
    fun editFilterAt(idx: Int)
    fun toggleFilterAt(idx: Int)

}
