package com.spotman.filter

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import com.spotman.RemovableList

class FiltersItemTouchHelperCallback(private val model: RemovableList) : ItemTouchHelper.SimpleCallback(0, 4 or 8) {

    override fun onMove(rv: RecyclerView, vh1: RecyclerView.ViewHolder, vh2: RecyclerView.ViewHolder): Boolean = false

    override fun onSwiped(vh: RecyclerView.ViewHolder, dir: Int) {
        model.removeAt(vh.adapterPosition)
    }
}
