package com.spotman.filter

import android.graphics.Paint
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.spotman.ListObserver
import com.spotman.R
import com.spotman.ReadableList

class FiltersAdapter(private val model: ReadableList<FilterMeta>, private val callback: FiltersAdapterCallback) :
    RecyclerView.Adapter<FilterViewHolder>(),
    ListObserver<FilterMeta> {

    override fun onCreateViewHolder(container: ViewGroup, idx: Int): FilterViewHolder {
        val view = LayoutInflater.from(container.context).inflate(R.layout.filters_vh, container, false)
        val vh = FilterViewHolder(view)

        vh.itemView.setOnClickListener {
            callback.toggleFilterAt(vh.adapterPosition)
        }

        vh.itemView.setOnLongClickListener {
            callback.editFilterAt(vh.adapterPosition)
            true
        }

        return vh
    }

    override fun getItemCount(): Int = model.length()

    override fun onBindViewHolder(vh: FilterViewHolder, idx: Int) {
        val filter = model.get(idx)
        vh.filterAttr.text = filter.attrKey()
        vh.filterValue.text = filter.attrVal()

        if (filter.toggled()) {
            vh.filterAttr.paintFlags = vh.filterAttr.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
            vh.filterValue.paintFlags = vh.filterValue.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
        } else {
            vh.filterAttr.paintFlags = vh.filterAttr.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            vh.filterValue.paintFlags = vh.filterValue.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        }
    }

    override fun onDataSetChanged(old: List<FilterMeta>, new: List<FilterMeta>) {
        val call = object : DiffUtil.Callback() {

            override fun getOldListSize(): Int = old.size

            override fun getNewListSize(): Int = new.size

            override fun areItemsTheSame(oIdx: Int, nIdx: Int): Boolean {
                return old[oIdx].attrKey() == new[nIdx].attrKey() && old[oIdx].attrVal() == new[nIdx].attrVal() && old[oIdx].toggled() == new[nIdx].toggled()
            }

            override fun areContentsTheSame(oIdx: Int, nIdx: Int): Boolean {
                return old[oIdx].attrKey() == new[nIdx].attrKey() && old[oIdx].attrVal() == new[nIdx].attrVal() && old[oIdx].toggled() == new[nIdx].toggled()
            }
        }
        DiffUtil.calculateDiff(call, false).dispatchUpdatesTo(this)
    }
}
