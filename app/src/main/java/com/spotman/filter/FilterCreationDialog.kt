package com.spotman.filter

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.widget.SpinnerAdapter
import com.spotman.R
import kotlinx.android.synthetic.main.filters_creation.view.*

const val FILTERS_CREATION_TAG = "FILTERS_CREATION_TAG"
const val FILTERS_CREATION_ATTRIBUTE_KEY = "FILTERS_CREATION_ATTRIBUTE_KEY"
const val FILTERS_CREATION_VALUE_KEY = "FILTERS_CREATION_VALUE_KEY"

class FilterCreationDialog : DialogFragment() {

    var onFilterCreationFinishedListener: (CharSequence, CharSequence) -> Unit = { _, _ -> }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val args = arguments
        val view = LayoutInflater.from(context).inflate(R.layout.filters_creation, null, false)
        val builder = AlertDialog.Builder(context)

        if (args?.containsKey(FILTERS_CREATION_VALUE_KEY) == true) {

            val attr = args.getString(FILTERS_CREATION_ATTRIBUTE_KEY)

            val idx = resources.getStringArray(R.array.filter_attributes).indexOf(attr)
            if(idx >= 0)
                view.filters_creation_attribute.setSelection(idx)

            view.filters_creation_value.setText(args.getString(FILTERS_CREATION_VALUE_KEY))

            builder.setTitle(R.string.filters_creation_title_update)
        } else {
            builder.setTitle(R.string.filters_creation_title_create)
        }

        builder.setView(view)

        builder.setPositiveButton(android.R.string.ok) { dialog, _ ->
            dialog.dismiss()
            val attribute = view.filters_creation_attribute.selectedItem.toString()
            val value = view.filters_creation_value.text.toString()
            onFilterCreationFinishedListener(attribute, value)
        }

        builder.setNegativeButton(android.R.string.cancel) { dialog, _ -> dialog.dismiss()}

        return builder.create()
    }
}
