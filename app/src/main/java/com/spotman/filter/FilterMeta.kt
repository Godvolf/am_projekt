package com.spotman.filter

interface FilterMeta {
    fun attrKey(): CharSequence
    fun attrVal(): CharSequence
    fun toggled(): Boolean

}
