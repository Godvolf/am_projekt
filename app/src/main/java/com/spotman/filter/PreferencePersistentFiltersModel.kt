package com.spotman.filter

import android.content.SharedPreferences
import com.spotman.ObservableList
import com.spotman.PreferencePersistent
import com.spotman.ReadableList
import com.spotman.RemovableList

interface PreferencePersistentFiltersModel: RemovableList, ObservableList<FilterMeta>, ReadableList<FilterMeta>, PreferencePersistent {
    fun createFilter(attribute: CharSequence, value: CharSequence)
    fun updateFilterAt(idx: Int, attribute: CharSequence, value: CharSequence)
    fun toggleFilterAt(idx: Int)
    fun restore(preferences: SharedPreferences)
}
