package com.spotman.filter

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.filters_vh.view.*

class FilterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    val filterAttr = itemView.filters_attribute
    val filterValue = itemView.filters_value
}
