package com.spotman

import com.spotman.data.SongUri
import com.spotman.facade.DestinationsFacade
import com.spotman.push.AbstractDestinationsFacade
import com.spotman.service.CallbackData

abstract class DestinationsFacadeProxy : AbstractDestinationsFacade {

    abstract var wrapped: AbstractDestinationsFacade

    fun activate(token: String) {
        wrapped = DestinationsFacade(token)
    }

    override fun createNewPlaylist(
        name: String, description: String, songUriList: List<SongUri>, callback: CallbackData<String>
    ) {
        wrapped.createNewPlaylist(name, description, songUriList, callback)
    }

    override fun addSongsToPlaylist(playlistId: String, songUriList: List<SongUri>, callback: CallbackData<String>) {
        wrapped.addSongsToPlaylist(playlistId, songUriList, callback)
    }

    override fun removeMatchingSongsFromPlaylist(
        playlistId: String, songUriList: List<SongUri>, callback: CallbackData<String>
    ) {
        wrapped.removeMatchingSongsFromPlaylist(playlistId, songUriList, callback)
    }
}
