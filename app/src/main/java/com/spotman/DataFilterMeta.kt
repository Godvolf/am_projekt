package com.spotman

import android.content.SharedPreferences
import android.support.v4.util.ArraySet
import com.spotman.filter.FILTERS_PREFERENCES_IDS_KEY
import com.spotman.filter.FilterMeta

class DataFilterMeta(var attribute: String, var value: String, var toggled: Boolean = true): FilterMeta, PreferencePersistent {

    override fun attrKey(): CharSequence = attribute

    override fun attrVal(): CharSequence = value

    override fun toggled(): Boolean = toggled

    override fun save(preferences: SharedPreferences) {
        val id = hashCode()
        preferences.edit()
            .putString("${id}_attribute", attribute)
            .putString("${id}_value", value)
            .putBoolean("${id}_toggled", toggled)
            .apply()
        val new = ArraySet<String>(preferences.getStringSet(FILTERS_PREFERENCES_IDS_KEY, emptySet()))
        new.add(id.toString())
        preferences.edit().putStringSet(FILTERS_PREFERENCES_IDS_KEY, new).commit()
    }

    class FromPreferenceFactory: com.spotman.FromPreferenceFactory<DataFilterMeta> {

        override fun restoreFrom(key: String, preferences: SharedPreferences): DataFilterMeta {
            return DataFilterMeta(
                preferences.getString("${key}_attribute", "null")!!,
                preferences.getString("${key}_value", "null")!!,
                preferences.getBoolean("${key}_toggled", true)
            )
        }
    }
}
