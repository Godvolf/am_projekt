package com.spotman.databaseModule

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.spotman.templates.TEMPLATES_DATEFORMAT
import com.spotman.templates.TemplateMeta
import java.text.SimpleDateFormat
import java.util.*

@Entity//(tableName = "Playlists")
data class Playlist(
    @PrimaryKey(autoGenerate = true) var id: Long?,
    var description : String?,
    var ctime: String = SimpleDateFormat(TEMPLATES_DATEFORMAT).format(Calendar.getInstance().time)
    //@Embedded var songs: ArrayList<Song>?
): TemplateMeta {

    override fun ctime(dateformat: CharSequence): CharSequence = ctime

    override fun name(): CharSequence = description.toString()
}

