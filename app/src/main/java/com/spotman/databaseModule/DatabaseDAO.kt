package com.spotman.databaseModule

import android.arch.persistence.room.*

/*
@Dao
interface PlaylistDao {

    @Query("SELECT * from playlists")
    fun getAllPlaylist(): Array<Playlist>

    @Query("SELECT * from Playlists where id = :Id ")
    fun getId(Id: Int): Playlist

    @Insert(onConflict = 1)
    fun insert(playlist: Playlist)

    @Query("DELETE from Playlists")
    fun deleteAllPlaylist()

    @Query("DELETE from Playlists where id = :Id")
    fun deleteIndexPlaylist(Id: Int)

}
*/
@Dao
interface PlaylistSongJoinDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(playlistSongJoin: PlaylistSongJoin)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(song:Song)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(playlist:Playlist)

    @Query("""
           SELECT * FROM playlist
           INNER JOIN playlist_song_join
           ON playlist.id=playlist_song_join.playlistId
           WHERE playlist_song_join.songUri=:songUri
           """)
    fun getPlaylistsForSong(songUri: String): Array<Playlist>

    @Query("""
           SELECT * FROM song
           INNER JOIN playlist_song_join
           ON song.Uri=playlist_song_join.songUri
           WHERE playlist_song_join.playlistId=:playlistId
           """)
    fun getSongsForPlaylist(playlistId: Long): Array<Song>

    @Query("SELECT * FROM song")
    fun getAllSongs():Array<Song>

    @Query("SELECT * FROM playlist")
    fun getAllPlaylist():Array<Playlist>

    @Query("SELECT * FROM song where Uri = :Uri")
    fun getByIdSong(Uri: String):Song

    @Query("SELECT * FROM playlist where id = :id")
    fun getByIdPlaylist(id :Long):Playlist

    @Query("SELECT * FROM playlist_song_join")
    fun getAllJoins(): Array<PlaylistSongJoin>

    @Delete
    fun deletePlaylist(playlist: Playlist)

    @Delete
    fun deleteSongFromPlaylist(playlistSongJoin: PlaylistSongJoin)

   // @Delete
   // fun deleteSong(song: Song)

    //@Query("Delete From song")
    //fun deleteAllSong()

    @Query("Delete From playlist")
    fun deleteAllPlaylist()

    @Query("Delete From playlist_song_join")
    fun deleteAllPlaylistSong()

    @Query("DELETE FROM playlist_song_join WHERE playlistId = :id")
    fun deleteAllJoinsForPlaylist(id: Long)

    @Query("DELETE FROM song WHERE uri NOT IN (SELECT uri FROM playlist_song_join)")
    fun deleteOrphanSongs()
}

