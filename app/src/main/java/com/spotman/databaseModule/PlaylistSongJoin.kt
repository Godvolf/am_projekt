package com.spotman.databaseModule

import android.arch.persistence.room.*
import android.arch.persistence.room.ForeignKey.CASCADE

@Entity(tableName = "playlist_song_join",
    primaryKeys = arrayOf("playlistId","songUri"),
    foreignKeys = arrayOf(
        ForeignKey(entity = Playlist::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("playlistId")
            ,onDelete = CASCADE,onUpdate = CASCADE
        ),
        ForeignKey(entity = Song::class,
            parentColumns = arrayOf("uri"),
            childColumns = arrayOf("songUri")
            ,onDelete = CASCADE,onUpdate = CASCADE
        )
    )
)
data class PlaylistSongJoin(
    val playlistId: Long,
    val songUri: String
)