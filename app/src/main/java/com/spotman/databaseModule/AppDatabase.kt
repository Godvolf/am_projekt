package com.spotman.databaseModule

import android.arch.persistence.room.*
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(entities =arrayOf(PlaylistSongJoin::class, Song::class, Playlist::class)    ,version = 3)
abstract class AppDatabase : RoomDatabase() {

    //abstract fun songDao(): SongDao
    //abstract fun  playlistDao(): PlaylistDao
    abstract fun playlistSongJoinDao(): PlaylistSongJoinDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext,
                        AppDatabase::class.java, "SpotmanDatabase").allowMainThreadQueries().build()


                }
            }

            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}