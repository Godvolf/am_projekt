package com.spotman.databaseModule


import android.content.Context
import android.support.v4.util.ArraySet
import com.spotman.ListObserver
import com.spotman.browser.DataSongMeta
import com.spotman.templates.TemplateMeta
import com.spotman.templates.TemplatesModel
import java.util.*

class DatabaseMenager(context: Context) : TemplatesModel<DataSongMeta> {

    private val databaseCaller = DatabaseCaller(context)
    private val list = ArrayList<Playlist>()
    private val observers = ArraySet<ListObserver<TemplateMeta>>()

    init {
        list.addAll(databaseCaller.getAllPlaylist())
    }

    override fun fetchAllOf(idx: Int): Iterable<DataSongMeta> {
        return databaseCaller.getSongsForPlaylist(list[idx].id!!).map {
            DataSongMeta(
                title = it.title,
                artist = it.artist,
                album = it.album,
                uri = it.uri
            )
        }
    }

    override fun createTemplate(name: String, time: Date, elems: Iterable<DataSongMeta>) {
        val id = databaseCaller.getAllPlaylist().map {it.id}.sortedBy { it }.fold(0.toLong()) { acc, id ->
            if(acc == id) acc + 1 else acc
        } // TODO czy poprawne znajdowanie id?

        databaseCaller.insert(Playlist(id, name))
        elems
            .map { Song(it.title, it.artist, it.album, "null", it.uri)}
            .forEach {
                databaseCaller.insert(it)
                databaseCaller.insert(PlaylistSongJoin(id, it.uri))
            }

        val old = ArrayList(list)
        list.clear()
        list.addAll(databaseCaller.getAllPlaylist())
        observers.forEach { it.onDataSetChanged(old, list) }
    }

    override fun length(): Int = list.size

    override fun get(idx: Int): TemplateMeta = list[idx]

    /*
     * - Delete the playlist
     * - Delete all its joins
     * - Delete all songs without a join
     */
    override fun removeAt(idx: Int) {
        val playlist = list[idx]

        databaseCaller.deleteJoinsForPlaylist(playlist.id!!)
        databaseCaller.deleteOrphanSongs()
        databaseCaller.deletePlaylist(playlist)

        val old = ArrayList(list)
        list.removeAt(idx)
        observers.forEach { it.onDataSetChanged(old, list) }
    }

    override fun register(observer: ListObserver<TemplateMeta>) {
        observers.add(observer)
    }

    override fun unregister(observer: ListObserver<TemplateMeta>) {
        observers.remove(observer)
    }
}