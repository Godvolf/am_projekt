package com.spotman.databaseModule

import android.arch.persistence.room.*
import android.net.Uri
import android.widget.ImageView
import com.spotman.SongMeta

@Entity//(tableName = "Songs")
data class Song (
     var title : String,
     var artist : String,
     var album : String,
     var image_href : String,
    @PrimaryKey var uri : String // chyba musi być string bo nie wie jak zmagazynaować URI
): SongMeta {

    override fun title(): CharSequence = title

    override fun artist(): CharSequence = artist

    override fun album(): CharSequence = album

    override fun drawThumbnailOn(view: ImageView) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun picturePath(): CharSequence{
        return this.image_href
    }

    override fun trackUri(): Uri {
       // return this.uri!!
        return Uri.parse(this.uri)
    }

}