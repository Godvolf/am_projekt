package com.spotman.databaseModule

import android.content.Context

class DatabaseCaller(context: Context) {

    private val database = AppDatabase.getInstance(context)!!

    fun getAllPlaylist():Array<Playlist>{
        return database.playlistSongJoinDao().getAllPlaylist()
    }

    fun getAllSongs():Array<Song>{
        return  database.playlistSongJoinDao().getAllSongs()
    }

    fun getByIdPlaylist(id:Long) :Playlist{
        return database.playlistSongJoinDao().getByIdPlaylist(id)
    }

    fun getByIdSong(uri:String) :Song{
        return database.playlistSongJoinDao().getByIdSong(uri)
    }

    fun getSongsForPlaylist(id: Long):Array<Song> {
        return database.playlistSongJoinDao().getSongsForPlaylist(id)
    }

    fun insert(playlist: Playlist){
        database.playlistSongJoinDao().insert(playlist)
    }
    fun insert(song: Song){
        database.playlistSongJoinDao().insert(song)
    }
    fun insert(joinPlaylistSongs: PlaylistSongJoin){
        database.playlistSongJoinDao().insert(joinPlaylistSongs)
    }

    fun deleteSongFromPlaylist(playlistSongJoin: PlaylistSongJoin){
       database.playlistSongJoinDao().deleteSongFromPlaylist(playlistSongJoin)
    }

    fun deletePlaylist(playlist: Playlist){
        database.playlistSongJoinDao().deletePlaylist(playlist)
    }

    fun deleteAllPlaylist(){
        database.playlistSongJoinDao().deleteAllPlaylist()
     //   database.playlistSongJoinDao().reindexPlaylist()
    }

    fun deleteAllPlaylistSong(){
        database.playlistSongJoinDao().deleteAllPlaylistSong()
    }

    fun getAllJoins(): Array<PlaylistSongJoin> = database.playlistSongJoinDao().getAllJoins()

    fun deleteJoinsForPlaylist(id: Long) {
        database.playlistSongJoinDao().deleteAllJoinsForPlaylist(id)
    }

    fun deleteOrphanSongs() {
        database.playlistSongJoinDao().deleteOrphanSongs()
    }

}
