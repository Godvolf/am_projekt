package com.spotman.push

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.spotman.ListObserver
import com.spotman.ObservableList
import com.spotman.R
import com.spotman.ReadableList

class PushAdapter(private val model: ReadableList<PlaylistMeta>, private val callback: PushAdapterCallback): RecyclerView.Adapter<PushViewHolder>(),
    ListObserver<PlaylistMeta> {

    override fun onCreateViewHolder(container: ViewGroup, idx: Int): PushViewHolder {
        val view = LayoutInflater.from(container.context).inflate(R.layout.push_vh, container, false)
        val vh = PushViewHolder(view)

        vh.supply.setOnClickListener {
            callback.supplyPlaylistAt(vh.adapterPosition)
        }

        vh.subtract.setOnClickListener {
            callback.subtractPlaylistAt(vh.adapterPosition)
        }

        return vh
    }

    override fun getItemCount(): Int = model.length()

    override fun onBindViewHolder(vh: PushViewHolder, idx: Int) {
        val playlist = model.get(idx)
        vh.title.text = playlist.title()
    }

    override fun onDataSetChanged(old: List<PlaylistMeta>, new: List<PlaylistMeta>) {
        val callback = object: DiffUtil.Callback() {

            override fun getOldListSize(): Int = old.size

            override fun getNewListSize(): Int = new.size

            override fun areItemsTheSame(o: Int, n: Int): Boolean {
                return old[o].title() == new[n].title()
            }

            override fun areContentsTheSame(o: Int, n: Int): Boolean {
                return old[o].title() == new[n].title()
            }
        }
        DiffUtil.calculateDiff(callback, false).dispatchUpdatesTo(this)
    }
}
