package com.spotman.push

interface PushAdapterCallback {
    fun supplyPlaylistAt(idx: Int)
    fun subtractPlaylistAt(idx: Int)

}
