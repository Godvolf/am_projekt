package com.spotman.push

import com.spotman.data.SongUri
import com.spotman.service.CallbackData

interface AbstractDestinationsFacade {
    fun createNewPlaylist(name: String, description: String, songUriList: List<SongUri>, callback: CallbackData<String>)
    fun addSongsToPlaylist(playlistId: String, songUriList: List<SongUri>, callback: CallbackData<String>)
    fun removeMatchingSongsFromPlaylist(playlistId: String, songUriList: List<SongUri>, callback: CallbackData<String>)

}
