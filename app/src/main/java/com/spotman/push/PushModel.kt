package com.spotman.push

import com.spotman.ObservableList
import com.spotman.ReadableList

interface PushModel: ReadableList<PlaylistMeta>, ObservableList<PlaylistMeta> {
    fun load(iterable: Iterable<PlaylistMeta>)
}
