package com.spotman.push

import com.spotman.data.Playlist
import com.spotman.service.CallbackData

interface AbstractOwnPlaylistFacade {
    fun fetchAllOwnPlaylists(callback: CallbackData<List<Playlist>>)

}
