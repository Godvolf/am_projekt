package com.spotman.push

import com.spotman.data.Playlist
import com.spotman.service.CallbackData

class NullOwnPlaylistFacade : AbstractOwnPlaylistFacade {
    override fun fetchAllOwnPlaylists(callback: CallbackData<List<Playlist>>) {}
}
