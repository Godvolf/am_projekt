package com.spotman.push

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.spotman.ElementProvider
import com.spotman.R
import com.spotman.data.Playlist
import com.spotman.data.SongUri
import com.spotman.service.CallbackData
import kotlinx.android.synthetic.main.push.view.*

abstract class PushFragment: Fragment(), PushAdapterCallback {

    var destinations: AbstractDestinationsFacade = NullDestinationsFacade()
    var playlists: AbstractOwnPlaylistFacade = NullOwnPlaylistFacade()
    var songs: ElementProvider<SongUri>? = null
    protected abstract val model: PushModel
    protected abstract val adapter: PushAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.push, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.push_recycler.setHasFixedSize(true)
        view.push_recycler.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        view.push_recycler.adapter = adapter

        view.push_create_button.setOnClickListener {
            val name = view.push_create_input.text.toString()
            val tracks = songs?.fetchAll()
            if(tracks != null && tracks.count() > 0) {
                destinations.createNewPlaylist(name, "", tracks.toList(), object : CallbackData<String> {
                    override fun onSuccess(value: String) {
                        Toast.makeText(context, context?.getString(R.string.push_toast_create_success, value), Toast.LENGTH_SHORT).show()
                    }

                    override fun onError(message: String) {
                        Toast.makeText(context, context?.getString(R.string.push_toast_create_error, message), Toast.LENGTH_LONG).show()
                    }
                })
            } else {
                Toast.makeText(context, R.string.push_toast_create_empty, Toast.LENGTH_SHORT).show()
            }
        }


        view.push_refresh.setOnClickListener {
            playlists.fetchAllOwnPlaylists(object : CallbackData<List<Playlist>> {
                override fun onSuccess(value: List<Playlist>) {
                    model.load(value)
                    Toast.makeText(context, context?.getString(R.string.push_refresh_toast_success), Toast.LENGTH_SHORT).show()
                }

                override fun onError(message: String) {
                    Toast.makeText(context, context?.getString(R.string.push_refresh_toast_error, message), Toast.LENGTH_LONG).show()
                }
            })
        }

        model.register(adapter)

    }

    override fun onDestroyView() {
        model.unregister(adapter)
        super.onDestroyView()
    }

    override fun supplyPlaylistAt(idx: Int) {
        val tracks = songs?.fetchAll()
        if(tracks != null && tracks.count() > 0) {
            destinations.addSongsToPlaylist(model.get(idx).playlistId().toString(), tracks.toList(),
                object : CallbackData<String> {
                    override fun onSuccess(value: String) {
                        Toast.makeText(context, context?.getString(R.string.push_toast_supply_success, value), Toast.LENGTH_SHORT).show()
                    }

                    override fun onError(message: String) {
                        Toast.makeText(context, context?.getString(R.string.push_toast_supply_error, message), Toast.LENGTH_LONG).show()
                    }
                })
        } else {
            Toast.makeText(context, R.string.push_toast_create_empty, Toast.LENGTH_SHORT).show()
        }
    }

    override fun subtractPlaylistAt(idx: Int) {
        val tracks = songs?.fetchAll()
        if(tracks != null && tracks.count() > 0) {
            destinations.removeMatchingSongsFromPlaylist(model.get(idx).playlistId().toString(), tracks.toList(),
                object : CallbackData<String> {
                    override fun onSuccess(value: String) {
                        Toast.makeText(context, context?.getString(R.string.push_toast_subtract_success, value), Toast.LENGTH_SHORT).show()
                    }

                    override fun onError(message: String) {
                        Toast.makeText(context, context?.getString(R.string.push_toast_subtract_error, message), Toast.LENGTH_LONG).show()
                    }
                })
        } else {
            Toast.makeText(context, R.string.push_toast_create_empty, Toast.LENGTH_SHORT).show()
        }
    }
}