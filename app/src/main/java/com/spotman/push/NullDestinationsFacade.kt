package com.spotman.push

import com.spotman.data.SongUri
import com.spotman.service.CallbackData

class NullDestinationsFacade : AbstractDestinationsFacade {
    override fun createNewPlaylist(
        name: String, description: String, tracks: List<SongUri>, callback: CallbackData<String>
    ) {}

    override fun addSongsToPlaylist(playlistId: String, tracks: List<SongUri>, callback: CallbackData<String>) {}

    override fun removeMatchingSongsFromPlaylist(
        playlistId: String, tracks: List<SongUri>, callback: CallbackData<String>
    ) {}
}
