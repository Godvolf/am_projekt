package com.spotman.push

import android.support.v7.widget.AppCompatImageButton
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.push_vh.view.*

class PushViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    val title: AppCompatTextView = itemView.push_vh_title
    val supply: AppCompatImageButton = itemView.push_vh_supply
    val subtract: AppCompatImageButton = itemView.push_vh_subtract
}
