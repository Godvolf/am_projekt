package com.spotman.push

interface PlaylistMeta {
    fun title(): CharSequence
    fun playlistId(): CharSequence

}
