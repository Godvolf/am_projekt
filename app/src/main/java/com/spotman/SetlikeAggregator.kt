package com.spotman

interface SetlikeAggregator<Aggregatee>: ClearableAggregate, ElementProvider<Aggregatee> {
    fun addUnique(elements: Iterable<Aggregatee>)
    fun removeMatching(elements: Iterable<Aggregatee>)
}
