package com.spotman.facade

import com.spotman.data.*
import com.spotman.push.AbstractOwnPlaylistFacade
import com.spotman.service.CallbackData
import com.spotman.service.CategoryService
import com.spotman.service.PlaylistService
import com.spotman.service.SearchService

class SourcesFacade (private var token : String) : AbstractOwnPlaylistFacade {
    private val playlistService = PlaylistService(token)
    private val categoryService = CategoryService(token)
    private val searchService = SearchService(token)

    fun feachOwnPlaylist(callback: CallbackData<List<Playlist>>){
        playlistService.feachOwnPlaylist(callback)
    }

    fun feachTrackForPlaylistById(playListId : String, callback: CallbackData<List<Track>>){
        playlistService.feachTrackForPlaylistById(playListId,callback)
    }

    fun feachAllCategory(callback: CallbackData<List<Category>>){
        categoryService.feachAllCategory(callback)
    }

    fun feachTracksForAlbumById(albumId : String, callback: CallbackData<List<Track>>){
        categoryService.feachSongsForAlbumById(albumId,callback)
    }

    fun feachSearchAlbum(query: String ,callback: CallbackData<List<Album>>){
        searchService.feachSearchAlbum(query, callback)
    }

    fun feachSearchArtist(query: String ,callback: CallbackData<List<Artist>>){
        searchService.feachSearchArtists(query, callback)
    }

    fun feachSearchPlaylist(query: String ,callback: CallbackData<List<Playlist>>){
        searchService.feachSearchPlaylist(query, callback)
    }

    fun feachSearchTrack( query: String ,callback: CallbackData<List<Track>>){
        searchService.feachSearchTrack(query, callback)
    }

    override fun fetchAllOwnPlaylists(callback: CallbackData<List<Playlist>>) {
        feachOwnPlaylist(callback)
    }
}