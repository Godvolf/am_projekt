package com.spotman.facade

import com.spotman.data.Playlist
import com.spotman.data.SongUri
import com.spotman.data.User
import com.spotman.push.AbstractDestinationsFacade
import com.spotman.service.CallbackData
import com.spotman.service.CategoryService
import com.spotman.service.PlaylistService

class DestinationsFacade (private var token : String) : AbstractDestinationsFacade {
    private val playlistService = PlaylistService(token)
    private val categoryService = CategoryService(token)

    override fun addSongsToPlaylist(playlistId: String, songUriList: List<SongUri>, callback : CallbackData<String>){
        playlistService.addSongsToPlaylist(playlistId, songUriList, callback)
    }

    override fun removeMatchingSongsFromPlaylist(playlistId: String, songUriList: List<SongUri>, callback : CallbackData<String>){
        playlistService.deleteSongsFromPlaylist(playlistId, songUriList, callback)
    }

    override fun createNewPlaylist(name: String, description: String, songUriList: List<SongUri>,callback : CallbackData<String>){
        categoryService.feachOwnUserData(object :
            CallbackData<User> {
            override fun onSuccess(value: User) {
                playlistService.createNewPlayList(value.id, name, description, object : CallbackData<Playlist>{
                    override fun onSuccess(playlist: Playlist){
                        playlistService.addSongsToPlaylist(playlist.id, songUriList, callback)
                    }

                    override fun onError(message: String) {
                        callback.onError(message)
                    }
                })
            }

            override fun onError(message: String) {
                System.out.println("Error")
            }
        })
    }
}
