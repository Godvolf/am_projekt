package com.spotman.service


interface CallbackData<T> {
    fun onSuccess(value: T)

    fun onError(message: String)
}