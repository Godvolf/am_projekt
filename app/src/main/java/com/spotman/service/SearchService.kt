package com.spotman.service

import com.spotman.data.Album
import com.spotman.data.Artist
import com.spotman.data.Playlist
import com.spotman.data.Track
import com.spotman.dto.search.SearchAllResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class SearchService (private var token : String) {

    private val spotifyApi : SpotifyApi
    private val authHeader : String

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.spotify.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        spotifyApi = retrofit.create(SpotifyApi::class.java)
        authHeader = "Bearer $token"
    }

    fun feachSearchArtists(query : String ,callback: CallbackData<List<Artist>>){
        val call = spotifyApi.searchArtists(authHeader, query)
        call.enqueue(object : Callback<SearchAllResponse>{
            override fun onFailure(call: Call<SearchAllResponse>, t: Throwable) {
                callback.onError("Bad request for search artist. " + t.message)
            }

            override fun onResponse(call: Call<SearchAllResponse>, response: Response<SearchAllResponse>) {
                val body = response.body()
                if(body != null){

                    var artistsLists = ArrayList<Artist>()
                    for(artistDTO in body.artists.items){
                        artistsLists.add(MapperService.mapArtistDtoToArtist(artistDTO))
                    }

                    callback.onSuccess(artistsLists)
                }
                else{
                    callback.onError("Body search Artist is null")
                }
            }

        })
    }

    fun feachSearchPlaylist(query : String ,callback: CallbackData<List<Playlist>>){
        val call = spotifyApi.searchPlaylist(authHeader, query)
        call.enqueue(object : Callback<SearchAllResponse>{
            override fun onFailure(call: Call<SearchAllResponse>, t: Throwable) {
                callback.onError("Bad request for search playlist. " + t.message)
            }

            override fun onResponse(call: Call<SearchAllResponse>, response: Response<SearchAllResponse>) {
                val body = response.body()
                if(body != null){

                    var playlistList = ArrayList<Playlist>()
                    for(playlistDTO in body.playlists.items){
                        playlistList.add(MapperService.mapPlaylistDtoToPlayList(playlistDTO))
                    }

                    callback.onSuccess(playlistList )
                }
                else{
                    callback.onError("Body search Playlist is null")
                }
            }

        })
    }

    fun feachSearchAlbum(query : String ,callback: CallbackData<List<Album>>){
        val call = spotifyApi.searchAlbum(authHeader, query)
        call.enqueue(object : Callback<SearchAllResponse>{
            override fun onFailure(call: Call<SearchAllResponse>, t: Throwable) {
                callback.onError("Bad request for search album. " + t.message)
            }

            override fun onResponse(call: Call<SearchAllResponse>, response: Response<SearchAllResponse>) {
                val body = response.body()
                if(body != null){

                    var albumLists = ArrayList<Album>()
                    for(albumDTO in body.albums.items){
                        albumLists.add(MapperService.mapAlbumDtoToAlbum(albumDTO))
                    }

                    callback.onSuccess(albumLists)
                }
                else{
                    callback.onError("Body search Album is null")
                }
            }

        })
    }

    fun feachSearchTrack(query : String ,callback: CallbackData<List<Track>>){
        val call = spotifyApi.searchTrack(authHeader, query)
        call.enqueue(object : Callback<SearchAllResponse>{
            override fun onFailure(call: Call<SearchAllResponse>, t: Throwable) {
                callback.onError("Bad request for search track. " + t.message)
            }

            override fun onResponse(call: Call<SearchAllResponse>, response: Response<SearchAllResponse>) {
                val body = response.body()
                if(body != null){

                    var tracksList = ArrayList<Track>()
                    for(songsDTO in body.tracks.items){
                        tracksList.add(MapperService.mapSongDtoToTrack(songsDTO))
                    }

                    callback.onSuccess(tracksList)
                }
                else{
                    callback.onError("Body search Track is null")
                }
            }

        })
    }

}