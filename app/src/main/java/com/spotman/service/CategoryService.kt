package com.spotman.service


import com.spotman.data.Category
import com.spotman.data.Track
import com.spotman.data.User
import com.spotman.dto.UserDTO
import com.spotman.dto.album.AlbumResponse
import com.spotman.dto.category.CategoryResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class CategoryService(private var token : String) {

    private val spotifyApi : SpotifyApi
    private val authHeader : String

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.spotify.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        spotifyApi = retrofit.create(SpotifyApi::class.java)
        authHeader = "Bearer $token"
    }


    fun feachAllCategory(callback: CallbackData<List<Category>>){
        val call = spotifyApi.getAllCategories(authHeader)
        call.enqueue( object : Callback<CategoryResponse> {
            override fun onFailure(call: Call<CategoryResponse>, t: Throwable) {
                callback.onError("Bad request for Category. " + t.message)
            }

            override fun onResponse(call: Call<CategoryResponse>, response: Response<CategoryResponse>) {
                val body = response.body()
                if(body != null){
                    var categoryList = ArrayList<Category>()
                    for(categotyDTO in body.categories.items){
                        categoryList .add(MapperService.mapCategoryDtoToCategory(categotyDTO))
                    }
                    callback.onSuccess(categoryList)
                }
                else{
                    callback.onError("Body category response is null")
                }
            }

        })
    }

    fun feachSongsForAlbumById(albumId : String, callback: CallbackData<List<Track>>) {
        val call = spotifyApi.getAlbumById(authHeader, albumId)
        call.enqueue(object : Callback<AlbumResponse> {
            override fun onFailure(call: Call<AlbumResponse>, t: Throwable) {
                callback.onError("Bad request for songrequest. " + t.message)
            }

            override fun onResponse(call: Call<AlbumResponse>, response: Response<AlbumResponse>) {
                val body = response.body()
                if (body != null) {
                    var trackLists = ArrayList<Track>()
                    var album = MapperService.mapAlbumDtoToAlbum(body.getAlbumDTO())
                    for(songDTO in body.tracks.items){
                        trackLists.add(MapperService.mapSongDtoToTrackWithAlbum(songDTO,album))
                    }
                    callback.onSuccess(trackLists)
                } else {
                    callback.onError("Body playlist response is null")
                }
            }

        })
    }

    fun feachOwnUserData(callback: CallbackData<User>){
        val call = spotifyApi.getMe(authHeader)
        call.enqueue(object : Callback<UserDTO> {
            override fun onFailure(call: Call<UserDTO>, t: Throwable) {
                callback.onError("Bad request for me request. " + t.message)
            }

            override fun onResponse(call: Call<UserDTO>, response: Response<UserDTO>) {
                val body = response.body()
                if (body != null) {
                    var user = MapperService.mapUserDtoToUser(body)
                    callback.onSuccess(user)
                } else {
                    callback.onError("Body me response is null")
                }
            }

        })
    }

}