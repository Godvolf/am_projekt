package com.spotman.service

import com.spotman.dto.NewPlaylistRequest
import com.spotman.dto.PutResponse
import com.spotman.dto.search.SearchAllResponse
import com.spotman.dto.album.AlbumResponse
import com.spotman.dto.category.CategoryResponse
import com.spotman.dto.playlist.PlaylistResponse
import com.spotman.dto.PostRequest
import com.spotman.dto.UserDTO
import com.spotman.dto.playlist.PlaylistDTO
import com.spotman.dto.song.TrackResponse
import retrofit2.Call
import retrofit2.http.*


interface SpotifyApi {

    @GET("/v1/me")
    fun getMe(@Header("Authorization") authHeader : String) : Call<UserDTO>

    @GET("/v1/browse/categories?limit=40")
    fun getAllCategories(@Header("Authorization") authHeader : String) : Call<CategoryResponse>

    @GET("/v1/me/playlists?limit=50")
    fun getAllOwnPlaylist(@Header("Authorization") authHeader : String) : Call<PlaylistResponse>

    @GET("/v1/playlists/{playlistId}/tracks")
    fun getTracksForPlaylist(@Header("Authorization") authHeader : String, @Path("playlistId") playlistId: String): Call<TrackResponse>

    @GET("v1/albums/{albumId}")
    fun getAlbumById(@Header("Authorization") authHeader : String, @Path("albumId") albumId: String) : Call<AlbumResponse>


    @GET("/v1/search?type=album&limit=50")
    fun searchAlbum(@Header("Authorization") authHeader : String, @Query("q") query : String) : Call<SearchAllResponse>

    @GET("/v1/search?type=artist&limit=50")
    fun searchArtists(@Header("Authorization") authHeader : String, @Query("q") query : String) : Call<SearchAllResponse>

    @GET("/v1/search?&type=track&limit=50")
    fun searchTrack(@Header("Authorization") authHeader : String, @Query("q") query : String) : Call<SearchAllResponse>

    @GET("/v1/search?type=playlist&limit=50")
    fun searchPlaylist(@Header("Authorization") authHeader : String, @Query("q") query : String) : Call<SearchAllResponse>

    @POST("/v1/users/{user_id}/playlists")
    fun createNewPlaylist(@Header("Authorization") authHeader : String, @Path("user_id" ) userId: String, @Body post : NewPlaylistRequest) : Call<PlaylistDTO>

    @POST("/v1/playlists/{playlistId}/tracks")
    fun addTrackListToPlaylist(@Header("Authorization") authHeader : String, @Path("playlistId" ) playlistId: String, @Body post : PostRequest) : Call<PutResponse>

    @HTTP(method = "DELETE", path = "/v1/playlists/{playlistId}/tracks", hasBody = true)
    fun deleteTrackListFromPlaylist(@Header("Authorization") authHeader : String, @Path("playlistId" ) playlistId: String, @Body post : PostRequest) : Call<PutResponse>

}

