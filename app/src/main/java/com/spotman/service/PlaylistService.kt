package com.spotman.service

import com.spotman.data.Playlist
import com.spotman.data.SongUri
import com.spotman.data.Track
import com.spotman.dto.NewPlaylistRequest
import com.spotman.dto.PostRequest
import com.spotman.dto.PutResponse
import com.spotman.dto.playlist.PlaylistDTO
import com.spotman.dto.playlist.PlaylistResponse
import com.spotman.dto.song.TrackResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class PlaylistService(private var token : String) {

    private val spotifyApi : SpotifyApi
    private val authHeader : String

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.spotify.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        spotifyApi = retrofit.create(SpotifyApi::class.java)
        authHeader = "Bearer $token"
    }

    fun feachOwnPlaylist(callback: CallbackData<List<Playlist>>){
        val call = spotifyApi.getAllOwnPlaylist(authHeader)
        call.enqueue( object : Callback<PlaylistResponse> {
            override fun onFailure(call: Call<PlaylistResponse>, t: Throwable) {
                callback.onError("Bad request for playlist. " + t.message)
            }

            override fun onResponse(call: Call<PlaylistResponse>, response: Response<PlaylistResponse>) {
                val body = response.body()
                if(body != null){

                    var playLists = ArrayList<Playlist>()
                    for(playlistDTO in body.items){
                        playLists.add(MapperService.mapPlaylistDtoToPlayList(playlistDTO))
                    }

                    callback.onSuccess(playLists)
                }
                else{
                    callback.onError("Body playlist response is null")
                }
            }

        })
    }

    fun feachTrackForPlaylistById(playListId : String, callback: CallbackData<List<Track>>){
        var call = spotifyApi.getTracksForPlaylist(authHeader, playListId)
        call.enqueue( object : Callback<TrackResponse> {
            override fun onFailure(call: Call<TrackResponse>, t: Throwable) {
                callback.onError("Bad request for songrequest. " + t.message)
            }

            override fun onResponse(call: Call<TrackResponse>, response: Response<TrackResponse>) {
                val body = response.body()
                if(body != null){
                    var trackLists = ArrayList<Track>()
                    for(trackDTO in body.items){
                        trackLists.add(MapperService.mapSongDtoToTrack(trackDTO.track))
                    }
                    callback.onSuccess(trackLists)
                }
                else{
                    callback.onError("Body playlist response is null")
                }
            }

        })
    }

    fun addSongsToPlaylist(playlistId : String, songUriList: List<SongUri>, callback : CallbackData<String>){
        var uriList = ArrayList<String>()

        for(song in songUriList){
            uriList.add(song.getSongUri())
        }

        var call = spotifyApi.addTrackListToPlaylist(authHeader, playlistId, PostRequest(uriList))
        call.enqueue( object : Callback<PutResponse> {
            override fun onFailure(call: Call<PutResponse>, t: Throwable) {
                callback.onError("Error " + t.message)
            }

            override fun onResponse(call: Call<PutResponse>, response: Response<PutResponse>) {
                val body = response.body()
                if(body != null){
                    callback.onSuccess("Added song to playlist")
                }
                else{
                    callback.onError(response.code().toString() + " " + response.message())
                }
            }

        })
    }


    fun deleteSongsFromPlaylist(playlistId : String, songUriList: List<SongUri>, callback : CallbackData<String>){
        var uriList = ArrayList<String>()

        for(song in songUriList){
            uriList.add(song.getSongUri())
        }

        var call = spotifyApi.deleteTrackListFromPlaylist(authHeader, playlistId, PostRequest(uriList))
        call.enqueue( object : Callback<PutResponse> {
            override fun onFailure(call: Call<PutResponse>, t: Throwable) {
                callback.onError("Error " + t.message)
            }

            override fun onResponse(call: Call<PutResponse>, response: Response<PutResponse>) {
                val body = response.body()
                if(body != null){
                    callback.onSuccess("Delete song from playlist")
                }
                else{
                    callback.onError(response.code().toString() + " " + response.message())
                }
            }

        })
    }

    fun createNewPlayList(userId : String, name : String, description : String, callback : CallbackData<Playlist>){
        val call = spotifyApi.createNewPlaylist(authHeader,userId, NewPlaylistRequest(name, description))
        call.enqueue( object : Callback<PlaylistDTO> {
            override fun onFailure(call: Call<PlaylistDTO>, t: Throwable) {
                callback.onError("Error " + t.message)
            }

            override fun onResponse(call: Call<PlaylistDTO>, response: Response<PlaylistDTO>) {
                val body = response.body()
                if(body != null){
                    callback.onSuccess(MapperService.mapPlaylistDtoToPlayList(body))
                }
                else{
                    callback.onError(response.code().toString() + " " + response.message())
                }
            }

        })
    }





}