package com.spotman.service

import com.spotman.data.*
import com.spotman.dto.artists.ArtistsDTO
import com.spotman.dto.ImageDTO
import com.spotman.dto.UserDTO
import com.spotman.dto.album.AlbumDTO
import com.spotman.dto.category.CategoryDTO
import com.spotman.dto.playlist.PlaylistDTO
import com.spotman.dto.song.SongDTO

class MapperService {

    companion object {

        fun mapPlaylistDtoToPlayList(playlistDTO: PlaylistDTO): Playlist {
            return Playlist.builder()
                .withId(playlistDTO.id)
                .withName(playlistDTO.name)
                .withImages(mapImageDtoListToImageList(playlistDTO.images))
                .withOwner(mapUserDtoToUser(playlistDTO.owner))
                .withPublic(playlistDTO.public)
                .withUri(playlistDTO.uri)
                .build()
        }

        fun mapImageDtoListToImageList(imageDTO: List<ImageDTO>): List<Image> {
            var images = ArrayList<Image>()
            for (img in imageDTO) {
                images.add(Image(img.height, img.width, img.url))
            }
            return images
        }

        fun mapUserDtoToUser(userDTO: UserDTO) : User {
            return User(userDTO.id, userDTO.display_name, userDTO.uri)
        }

        fun mapArtistDtoToArtist(artistsDTO: ArtistsDTO) : Artist{
            return Artist(artistsDTO.id, artistsDTO.name, artistsDTO.uri)
        }

        fun mapArtistDtoListToArtistList(artistsDTO: List<ArtistsDTO>): List<Artist> {
            var artists = ArrayList<Artist>()
            for (a in artistsDTO) {
                artists.add(mapArtistDtoToArtist(a))
            }
            return artists
        }

        fun mapAlbumDtoToAlbum(albumDTO: AlbumDTO) : Album {
            return Album.builder()
                .withId(albumDTO.id)
                .withImages(mapImageDtoListToImageList(albumDTO.images))
                .withName(albumDTO.name)
                .withReleaseDate(albumDTO.release_date)
                .withUri(albumDTO.uri)
                .withArtists(mapArtistDtoListToArtistList(albumDTO.artists))
                .build()
        }

        fun mapCategoryDtoToCategory(categoryDTO: CategoryDTO) : Category{
            return Category(categoryDTO.id, categoryDTO.name,
                mapImageDtoListToImageList(categoryDTO.icons)
            )
        }

        fun mapSongDtoToTrack(songDTO: SongDTO) : Track{
            return Track.builder()
                .withId(songDTO.id)
                .withName(songDTO.name)
                .withAlbum(mapAlbumDtoToAlbum(songDTO.album))
                .withArtists(mapArtistDtoListToArtistList(songDTO.artists))
                .withUri(songDTO.uri)
                .withPopularity(songDTO.popularity)
                .withDurationMs(songDTO.duration_ms)
                .build()
        }

        fun mapSongDtoToTrackWithAlbum(songDTO: SongDTO, album: Album) : Track{
            return Track.builder()
                .withId(songDTO.id)
                .withName(songDTO.name)
                .withAlbum(album)
                .withArtists(mapArtistDtoListToArtistList(songDTO.artists))
                .withUri(songDTO.uri)
                .withPopularity(songDTO.popularity)
                .withDurationMs(songDTO.duration_ms)
                .build()
        }

    }
}