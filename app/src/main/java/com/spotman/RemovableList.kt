package com.spotman

interface RemovableList {
    fun removeAt(idx: Int)
}
