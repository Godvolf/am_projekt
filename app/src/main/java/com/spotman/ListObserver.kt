package com.spotman

interface ListObserver<in DataType> {
    fun onDataSetChanged(old: List<DataType>, new: List<DataType>)
}
