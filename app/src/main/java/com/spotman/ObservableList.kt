package com.spotman

interface ObservableList<out DataType> {
    fun register(observer: ListObserver<DataType>)
    fun unregister(observer: ListObserver<DataType>)
}