package com.spotman

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.spotify.sdk.android.authentication.AuthenticationClient
import com.spotify.sdk.android.authentication.AuthenticationRequest
import com.spotify.sdk.android.authentication.AuthenticationResponse
import com.spotman.databaseModule.*
import kotlinx.android.synthetic.main.auth.*

abstract class AuthActivity : AppCompatActivity() {

    private var accessToken = ""

    abstract val someBrowserActivity: Class<*>


//    lateinit var databaseMenager : DatabaseCaller

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.auth)

     //   testDatabase()

        auth_auth.setOnClickListener {
            authorize()
        }

        auth_skip.setOnClickListener {
            val toBrowser = Intent(this, someBrowserActivity)
            startActivity(toBrowser)
        }

        askForPermissions()
    }

    private fun authorize() {
        val clientId = getString(R.string.client_id)
        val redirectUri = getString(R.string.redirect_uri)
        val scopes = resources.getStringArray(R.array.privileges)

        val request = AuthenticationRequest.Builder(clientId, AuthenticationResponse.Type.TOKEN, redirectUri)
            .setScopes(scopes)
            .build()

        AuthenticationClient.openLoginActivity(this, REQUEST_AUTH_CODE, request)
    }

    private fun askForPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED
            || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
            != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE),
                ASK_PERMISSION_REQUEST_CODE
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)

        if (requestCode == REQUEST_AUTH_CODE) {
            val response = AuthenticationClient.getResponse(resultCode, intent)

            if (response.type == AuthenticationResponse.Type.TOKEN) {
                accessToken = response.accessToken
                val toBrowser = Intent(this, someBrowserActivity)
                toBrowser.putExtra(AUTH_ACCESSTOKEN_KEY, accessToken)
                startActivity(toBrowser)
            }
        }
    }



/*
    fun testDatabase(){
        databaseMenager = DatabaseCaller(applicationContext)

      //databaseMenager.deleteAllPlaylist()
        databaseMenager.insert(Song("time","Zimmer","incepcja","sefsefsef", "Uri 1"))
        databaseMenager.insert(Song("piraci","Zimmer","karaiby","sehjsbfsffsefsef","Uri 2"))
        databaseMenager.insert(Song("makła księżnicka","disne","sowwithe","sefsef","Uri 3"))
        var s = ""
        databaseMenager.getAllSongs().forEach { s = s + " | " + it.toString() }
        Log.d("DATABASE list",s)
        val tsLong = System.currentTimeMillis() / 1000
        val ts = tsLong.toString()
        databaseMenager.insert(Playlist(null,"pierwsza playlista",(System.currentTimeMillis() / 1000).toString()))
        databaseMenager.insert(PlaylistSongJoin(1,"Uri 1"))
        databaseMenager.insert(PlaylistSongJoin(1,"Uri 2"))
        databaseMenager.insert(Playlist(null,"druga playlista",(System.currentTimeMillis() / 1000).toString()))
        databaseMenager.insert(PlaylistSongJoin(2,"Uri 3"))
        databaseMenager.insert(PlaylistSongJoin(2,"Uri 1"))
        Log.d("DATABASE playlists",databaseMenager.getSongsForPlaylist(1).toString())
        s=""
        databaseMenager.getSongsForPlaylist(1).forEach { s = s +" | "+it.toString() }
        Log.d("DATABASE playlists song",s)
        Log.d("DATABASE playlists",databaseMenager.getSongsForPlaylist(2).toString())
        s=""
        databaseMenager.getSongsForPlaylist(2).forEach { s = s +" | "+it.toString() }
        Log.d("DATABASE playlists song",s)
        databaseMenager.deletePlaylist(databaseMenager.getByIdPlaylist(2))
        //databaseMenager.insert(playlist)
        //Log.d("DATABASE RECIVED",databaseMenager.getAllPlaylist()[0].toString())

    }
*/


}