package com.spotman

import android.support.v4.util.ArraySet
import com.spotman.push.PlaylistMeta
import com.spotman.push.PushModel

class ConcretePushModel : PushModel {

    private val list = ArrayList<PlaylistMeta>()
    private val observers = ArraySet<ListObserver<PlaylistMeta>>()

    override fun load(iterable: Iterable<PlaylistMeta>) {
        val old = ArrayList(list)
        list.clear()
        list.addAll(iterable)
        observers.forEach { it.onDataSetChanged(old, list) }
    }

    override fun length(): Int = list.size

    override fun get(idx: Int): PlaylistMeta = list[idx]

    override fun register(observer: ListObserver<PlaylistMeta>) {
        observers.add(observer)
    }

    override fun unregister(observer: ListObserver<PlaylistMeta>) {
        observers.remove(observer)
    }
}