package com.spotman.sources

interface TracksProviderCallback<Aggregatee> {
    fun onTracksReceived(tracks: Iterable<Aggregatee>)

}
