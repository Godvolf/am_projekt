package com.spotman.sources

import com.spotman.ApplicationExtended.Companion.globalToken
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.spotman.R
import com.spotman.browser.DataSongMeta
import com.spotman.data.Album
import com.spotman.data.Artist
import com.spotman.data.Playlist
import com.spotman.data.Track
import com.spotman.service.CallbackData
import com.spotman.service.SearchService

class MainSearchFragment: Fragment(), AdapterView.OnItemSelectedListener {

    val callback = TracksProviderCallbackWrapper()

    var rv: RecyclerView? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
        ): View? {

        val token = globalToken
        System.out.println(globalToken + " TUTAJ2")
        val service = SearchService(token)

        val view1: View =  inflater.inflate(R.layout.main_search_fragment, container, false)
        val spinnerValues = arrayOf("Albums", "Track", "Playlist")



        // Initializing an ArrayAdapter

        val spinner = view1.findViewById<Spinner>(R.id.spinner)
        val adapter = ArrayAdapter(
            context, // Context
            android.R.layout.simple_spinner_item, // Layout
            spinnerValues // Array
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner.adapter = adapter

        var btn : Button = view1.findViewById(R.id.button1)
        val clickListener = View.OnClickListener {_ ->

            var editText = view1.findViewById<EditText>(R.id.editText1)
            var input = editText.text.toString()

            var item = spinner.selectedItem.toString()
            var users = ArrayList<SearchElement>()
            when (item) {
                "Albums" ->
                    service.feachSearchAlbum(input, object : CallbackData<List<Album>> {
                        override fun onSuccess(value: List<Album>) {
                            var counter = 0
                            for(c : Album in value){
                                users.add(SearchElement(c.name, c.artists[0].name, c.id , ""))
                                counter ++

                                if(counter >= 20) {
                                    break
                                }
                            }
                            var adapter = SongsAdapter(users, callback, "Album")

                            rv!!.adapter = adapter
                        }

                        override fun onError(message: String) {
                            System.out.println("Error22")

                        }
                    })
                "Track" -> service.feachSearchTrack(input, object :
                    CallbackData<List<Track>> {
                        override fun onSuccess(value: List<Track>) {
                            var counter = 0
                            for(c : Track in value){
                                var artistsNames = c.artists[0].name
                                for (a: Artist in c.artists) {
                                    if(a.name == artistsNames) {

                                    } else {
                                        artistsNames += ", " + a.name
                                    }

                                }
                                users.add(SearchElement(c.name, artistsNames, c.uri, c.album.name))
                                counter ++


                                if(counter >= 20) {
                                    break
                                }
                            }
                            var adapter = SongsAdapter(users, callback, "Track")
                            rv!!.adapter = adapter
                        }

                        override fun onError(message: String) {
                            System.out.println("Error")
                        }
                    })
                "Playlist" -> service.feachSearchPlaylist(input, object :
                    CallbackData<List<Playlist>> {
                    override fun onSuccess(value: List<Playlist>) {
                        var counter = 0
                        for(c : Playlist in value){
                            users.add(SearchElement(c.name, c.owner.name, c.id , c.uri))
                            counter ++


                            if(counter >= 20) {
                                break
                            }
                        }
                        var adapter = SongsAdapter(users, callback, "Playlist")
                        rv!!.adapter = adapter
                    }

                    override fun onError(message: String) {
                        System.out.println("Error")
                    }
                })
            }


        }
        btn.setOnClickListener(clickListener)



        return view1
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rv = getView()?.findViewById<RecyclerView>(R.id.recyclerView)!!
        rv?.layoutManager = LinearLayoutManager(activity)
        val users = ArrayList<SearchElement>()

        if (rv != null) {
            rv!!.adapter = SongsAdapter(users, callback, "Empty")
        }
    }

    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {

    }

    override fun onNothingSelected(arg0: AdapterView<*>) {

    }


}

class TracksProviderCallbackWrapper: TracksProviderCallback<DataSongMeta> {

    var wrapped: TracksProviderCallback<DataSongMeta>? = null

    override fun onTracksReceived(tracks: Iterable<DataSongMeta>) {
        wrapped?.onTracksReceived(tracks)
    }
}