package com.spotman.sources

import com.spotman.ApplicationExtended.Companion.globalToken
import com.spotman.service.CategoryService
import com.spotman.service.PlaylistService
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.spotman.browser.DataSongMeta
import com.spotman.data.Album
import com.spotman.data.Artist
import com.spotman.data.Track
import com.spotman.service.CallbackData
import java.util.*
import kotlin.collections.ArrayList


class SongsAdapter(var userList: ArrayList<SearchElement>, private val callback: TracksProviderCallback<DataSongMeta>, var searchedCategory: String): RecyclerView.Adapter<SongsAdapter.ViewHolder>() {

    var trackList = ArrayList<SearchElement>()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder?.txtName?.text = userList[position].author
        holder?.txtTitle?.text = userList[position].title
        holder?.txt3?.text = userList[position].album
        holder?.txt4.text = userList[position].uri

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent?.context).inflate(com.spotman.R.layout.item, parent, false)
        val vh = ViewHolder(v)
        vh.itemView.setOnClickListener {
            var song = userList[vh.adapterPosition]
            if(searchedCategory == "Track") {
                callback.onTracksReceived(Collections.singleton(DataSongMeta(song.title, song.author, song.album, song.uri)))
                System.out.println("Hello there")
            } else if (searchedCategory == "Album"){
                // userList[vh.adapterPosition].album = ""
                val service = CategoryService(globalToken)
                trackList = ArrayList<SearchElement>()
                service.feachSongsForAlbumById(userList[vh.adapterPosition].uri, object: CallbackData<List<Track>> {
                    override fun onSuccess(value: List<Track>) {
                        var counter = 0
                        for(c : Track in value){
                            var artistsNames = c.artists[0].name
                            for (a: Artist in c.artists) {
                                if(a.name == artistsNames) {

                                } else {
                                    artistsNames += ", " + a.name
                                }

                            }
                            trackList.add(SearchElement(c.name, artistsNames, c.uri, c.album.name))
                            counter ++


                            if(counter >= 20) {
                                break
                            }
                        }
                        userList = trackList
                        updateView(vh, counter)
                        searchedCategory = "Track"
                    }

                    override fun onError(message: String) {
                        System.out.println("Error22")
                    }
                })
            } else if (searchedCategory == "Playlist"){
                val service = PlaylistService(globalToken)
                trackList = ArrayList<SearchElement>()
                service.feachTrackForPlaylistById(userList[vh.adapterPosition].uri, object: CallbackData<List<Track>> {
                    override fun onSuccess(value: List<Track>) {
                        var counter = 0
                        for(c : Track in value){
                            var artistsNames = c.artists[0].name
                            for (a: Artist in c.artists) {
                                if(a.name == artistsNames) {
                                } else {
                                    artistsNames += ", " + a.name
                                }
                            }
                            trackList.add(SearchElement(c.name, artistsNames, c.uri, c.album.name))
                            counter ++

                            if(counter >= 20) {
                                break
                            }
                        }
                        userList = trackList
                        updateView(vh, counter)
                        searchedCategory = "Track"
                    }

                    override fun onError(message: String) {
                        System.out.println("Error22")
                    }
                })
            }

        }
        return vh
    }

    fun updateView(holder: ViewHolder, size: Int) {
        for ( i in 0..userList.size-1) {
            onBindViewHolder(holder, i)

        }
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

        val txtName = itemView.findViewById<TextView>(com.spotman.R.id.txtName)!!
        val txtTitle = itemView.findViewById<TextView>(com.spotman.R.id.txtTitle)!!
        val txt3 = itemView.findViewById<TextView>(com.spotman.R.id.txt3)!!
        val txt4 = itemView.findViewById<TextView>(com.spotman.R.id.txt4)!!
    }
}