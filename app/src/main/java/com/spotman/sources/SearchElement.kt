package com.spotman.sources

data class SearchElement(var author: String, var title: String, var uri: String, var album: String)