package com.spotman

import android.net.Uri
import android.widget.ImageView

interface SongMeta {
    fun title(): CharSequence
    fun artist(): CharSequence
    fun album(): CharSequence
    fun drawThumbnailOn(view: ImageView)
    fun trackUri(): Uri
}
