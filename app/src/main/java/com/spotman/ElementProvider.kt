package com.spotman

interface ElementProvider<out DataType> {
    fun fetchAll(): Iterable<DataType>
}
