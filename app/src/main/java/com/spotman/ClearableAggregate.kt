package com.spotman

interface ClearableAggregate {
    fun clear()
}
