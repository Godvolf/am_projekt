package com.spotman.browser

interface TokenReceivedCallback {
    fun onTokenReceived(token: String)

}
