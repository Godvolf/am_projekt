package com.spotman.browser

import android.support.v7.widget.AppCompatImageButton
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import kotlinx.android.synthetic.main.browser_vh.view.*

class BrowserViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    val title: TextView = itemView.browser_vh_title
    val description: TextView = itemView.browser_vh_description
    val thumbnail: AppCompatImageView = itemView.browser_vh_thumbnail
    val play: AppCompatImageButton = itemView.browser_vh_play
}
