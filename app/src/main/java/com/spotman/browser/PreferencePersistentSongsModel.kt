package com.spotman.browser

import android.content.SharedPreferences
import com.spotman.*
import com.spotman.sources.TracksProviderCallback

interface PreferencePersistentSongsModel<DataType> : RemovableList, ObservableList<DataType>, ClearableAggregate,
    ReadableList<DataType>, PreferencePersistent, SetlikeAggregator<DataType>, TracksProviderCallback<DataType> {
    fun restore(preferences: SharedPreferences)
}
