package com.spotman.browser

import android.content.Intent
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.spotman.ListObserver
import com.spotman.R
import com.spotman.ReadableList
import com.spotman.SongMeta

class BrowserAdapter(private val data: ReadableList<SongMeta>, private val callback: BrowserAdapterCallback) :
    RecyclerView.Adapter<BrowserViewHolder>(), ListObserver<SongMeta> {

    override fun onCreateViewHolder(group: ViewGroup, idx: Int): BrowserViewHolder {
        val view = LayoutInflater.from(group.context).inflate(R.layout.browser_vh, group, false)
        val vh = BrowserViewHolder(view)
        vh.play.setOnClickListener {
            val toSpotify = Intent(Intent.ACTION_VIEW, data.get(vh.adapterPosition).trackUri())
            callback.startIntent(toSpotify)
        }
        return vh
    }

    override fun getItemCount(): Int = data.length()

    override fun onBindViewHolder(vh: BrowserViewHolder, idx: Int) {
        val song = data.get(idx)
        song.drawThumbnailOn(vh.thumbnail)
        vh.title.text = song.title()
        vh.description.text =
            vh.itemView.context.getString(R.string.browser_vh_description_template, song.artist(), song.album())
    }

    override fun onDataSetChanged(old: List<SongMeta>, new: List<SongMeta>) {

        val callback = object : DiffUtil.Callback() {

            override fun areItemsTheSame(oIdx: Int, nIdx: Int): Boolean {
                val a = old[oIdx]
                val b = new[nIdx]
                return a.title() == b.title() && a.artist() == b.artist() && a.album() == b.album()
            }

            override fun areContentsTheSame(oIdx: Int, nIdx: Int): Boolean {
                val a = old[oIdx]
                val b = new[nIdx]
                return a.title() == b.title() && a.artist() == b.artist() && a.album() == b.album()
            }

            override fun getOldListSize(): Int = old.size

            override fun getNewListSize(): Int = new.size
        }
        val result = DiffUtil.calculateDiff(callback, false)
        result.dispatchUpdatesTo(this)
    }
}