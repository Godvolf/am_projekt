package com.spotman.browser

import android.content.Intent

interface BrowserAdapterCallback {
    fun startIntent(intent: Intent)

}
