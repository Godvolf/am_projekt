package com.spotman.browser

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import com.spotman.RemovableList

class ItemTouchHelperBrowserCallback(private val model: RemovableList): ItemTouchHelper.SimpleCallback(0, 4 or 8) {

    override fun onMove(v: RecyclerView, vh1: RecyclerView.ViewHolder, vh2: RecyclerView.ViewHolder): Boolean {
        return false
    }

    override fun onSwiped(vh: RecyclerView.ViewHolder, dir: Int) {
        val idx = vh.adapterPosition
        model.removeAt(idx)
    }
}
