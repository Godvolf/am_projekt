package com.spotman.browser

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.v4.view.PagerAdapter
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.ActionMode
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.spotify.sdk.android.authentication.AuthenticationClient
import com.spotify.sdk.android.authentication.AuthenticationRequest
import com.spotify.sdk.android.authentication.AuthenticationResponse
import com.spotman.AUTH_ACCESSTOKEN_KEY
import com.spotman.R
import com.spotman.REQUEST_AUTH_CODE
import com.spotman.SongMeta
import kotlinx.android.synthetic.main.actions.*
import kotlinx.android.synthetic.main.browser.*
import com.spotman.ApplicationExtended.Companion.globalToken

const val BROWSER_PREFERENCES_SONG_KEY = "BROWSER_PREFERENCES_SONG_KEY"
const val BROWSER_PREFERENCES_SONG_IDS_KEY = "BROWSER_PREFERENCES_SONG_IDS_KEY"
const val BROWSER_AUTHORIZE_REQUESTCODE = 0x54e9

abstract class BrowserActivity<Aggregatee: SongMeta> : AppCompatActivity(), BrowserAdapterCallback {

    private var actionsHeader: View? = null
    private var behavior: BottomSheetBehavior<View>? = null
    private var actionMode: ActionMode? = null

    abstract val model: PreferencePersistentSongsModel<Aggregatee>
    abstract val pager: PagerAdapter
    abstract val adapter: BrowserAdapter
    abstract val callback: TokenReceivedCallback

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (intent.extras?.containsKey(AUTH_ACCESSTOKEN_KEY) == true) {
            val token = intent.extras!!.getString(AUTH_ACCESSTOKEN_KEY)!!
            globalToken = token
            callback.onTokenReceived(token)
        }

        setContentView(R.layout.browser)

        model.restore(getSharedPreferences(BROWSER_PREFERENCES_SONG_KEY, Context.MODE_PRIVATE))

        val callback = ItemTouchHelperBrowserCallback(model)
        val helper = ItemTouchHelper(callback)

        browser_recycler.setHasFixedSize(true)
        browser_recycler.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        browser_recycler.adapter = adapter
        helper.attachToRecyclerView(browser_recycler)

        actionsHeader = browser_actions

        behavior = BottomSheetBehavior.from(actions_sheet)
        behavior?.setBottomSheetCallback(BrowserBottomSheetBehaviorCallback())
        behavior?.state = BottomSheetBehavior.STATE_HIDDEN
        actionsHeader?.setOnClickListener {
            behavior?.state = BottomSheetBehavior.STATE_EXPANDED
            actionMode = startActionMode(ActionsActionModeCallback())
        }

        action_pager.adapter = pager
        actions_tabs.setupWithViewPager(action_pager)

        model.register(adapter)
    }

    override fun onStop() {
        model.save(getSharedPreferences(BROWSER_PREFERENCES_SONG_KEY, Context.MODE_PRIVATE))
        super.onStop()
    }

    override fun onDestroy() {
        model.unregister(adapter)
        super.onDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.browser, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.browser_clear -> {
                model.clear()
            }
            R.id.browser_filter -> {
                // toFilters
            }
            R.id.browser_history -> {
                // toHistory
            }
        }
        return true
    }

    inner class BrowserBottomSheetBehaviorCallback : BottomSheetBehavior.BottomSheetCallback() {

        override fun onSlide(v: View, offset: Float) {}

        override fun onStateChanged(v: View, state: Int) {
            if (state == BottomSheetBehavior.STATE_HIDDEN)
                actionMode?.finish()
        }
    }

    inner class ActionsActionModeCallback : ActionMode.Callback {

        override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
            return true
        }

        override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
            return true
        }

        override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
            return true
        }

        override fun onDestroyActionMode(mode: ActionMode?) {
            behavior?.state = BottomSheetBehavior.STATE_HIDDEN
        }
    }

    override fun startIntent(intent: Intent) {
        startActivity(intent)
    }
}
