package com.spotman.browser

import android.content.SharedPreferences
import android.net.Uri
import android.support.v4.util.ArraySet
import android.util.Log
import android.widget.ImageView
import com.spotman.FromPreferenceFactory
import com.spotman.PreferencePersistent
import com.spotman.R
import com.spotman.SongMeta
import com.spotman.data.SongUri

data class DataSongMeta(val title: String, val artist: String, val album: String, val uri: String) : SongMeta,
    PreferencePersistent, SongUri {
    override fun title(): CharSequence = title

    override fun artist(): CharSequence = artist

    override fun album(): CharSequence = album

    override fun drawThumbnailOn(view: ImageView) {
        view.setImageResource(R.mipmap.ic_launcher) // TODO Replace placeholder with actual bitmap
    }

    override fun trackUri(): Uri = Uri.parse(uri)

    override fun getSongUri(): String = uri

    override fun save(preferences: SharedPreferences) {
        val id = hashCode()
        preferences.edit()
            .putString("${id}_title", title)
            .putString("${id}_artist", artist)
            .putString("${id}_album", album)
            .putString("${id}_uri", uri)
            .apply()

        val ids = ArraySet<String>(preferences.getStringSet(BROWSER_PREFERENCES_SONG_IDS_KEY, emptySet()))
        ids.add(id.toString())
        preferences.edit().putStringSet(BROWSER_PREFERENCES_SONG_IDS_KEY, ids).commit()
    }
}

class DataSongMetaFromPreferencesFactory : FromPreferenceFactory<DataSongMeta> {

    override fun restoreFrom(key: String, preferences: SharedPreferences): DataSongMeta {
        return DataSongMeta(
            preferences.getString("${key}_title", "null") ?: "null",
            preferences.getString("${key}_artist", "null") ?: "null",
            preferences.getString("${key}_album", "null") ?: "null",
            preferences.getString("${key}_uri", "null") ?: "null"
        )
    }
}