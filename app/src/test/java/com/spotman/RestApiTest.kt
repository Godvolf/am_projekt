package com.spotman

import android.util.Log
import com.spotman.data.*
import com.spotman.dto.song.SongDTO
import com.spotman.facade.DestinationsFacade
import com.spotman.service.CallbackData
import com.spotman.service.CategoryService
import com.spotman.service.PlaylistService
import com.spotman.service.SearchService
import org.junit.Assert
import org.junit.Test

class RestApiTest {
    val token = "BQB-CxZR365kXCukdjcH1Ys4k-1A3DJtYssFwtC2rb02_L83rS9PhsK4psVRsnbtaGoYA0N-RA_GBpTpQIdS_q61oh1PSrkvrKDinr0vip-hDohDNSggG3auG7DMTLM8P9x2lJwkdtcuVnPst-W5lFQSumJwNESjgjWVV_23WewEmU5D_EH7qCvPmqMVkKw"

    @Test
    fun checkPlaylistService(){
        System.out.println("aaa")
        val service = PlaylistService(token)
        service.feachTrackForPlaylistById("6rLvrrmWnYaqoGyklv6DEp", object :
            CallbackData<List<Track>> {
            override fun onSuccess(value: List<Track>) {
                System.out.println("Playlist!!!!!")
                for(c : Track in value){
                    System.out.println(c.name)
                    System.out.println(c.artists[0].name)
                    System.out.println(c.album.name)
                }
            }
            override fun onError(message: String) {
                System.out.println("Error")
            }
        })

        Thread.sleep(3000)
    }

    @Test
    fun checkCategoryService(){
        val service = CategoryService(token)
        service.feachSongsForAlbumById("7jfNbUImln4VF01QtMpe0q", object :
            CallbackData<List<Track>> {
            override fun onSuccess(value: List<Track>) {
                System.out.println("Album!!!!!!")
                for(c : Track in value){
                    System.out.println(c.name)
                    System.out.println(c.uri)
                    System.out.println(c.artists[0].name)
                    System.out.println(c.album.name)
                }
            }

            override fun onError(message: String) {
                System.out.println("Error")
            }
        })

        Thread.sleep(3000)
    }

    @Test
    fun checkgetListPlaylist(){
        val service = PlaylistService(token)
        service.feachOwnPlaylist( object :
            CallbackData<List<Playlist>> {
            override fun onSuccess(value: List<Playlist>) {
                System.out.println("Album!!!!!!")
                for(c : Playlist in value){
                    System.out.println(c.name)
                    System.out.println(c.owner.name)
                    System.out.println(c.id)
                }
            }

            override fun onError(message: String) {
                System.out.println("Error")
            }
        })

        Thread.sleep(3000)
    }

    @Test
    fun searchAlbum(){
        val service = SearchService(token)
        service.feachSearchAlbum("the", object :
            CallbackData<List<Album>> {
            override fun onSuccess(value: List<Album>) {
                System.out.println("Album!!!!!!")
                for(c : Album in value){
                    System.out.println(c.name)
                    System.out.println(c.artists[0].name)
                    System.out.println(c.id)
                }
            }

            override fun onError(message: String) {
                System.out.println("Error")
            }
        })

        Thread.sleep(3000)
    }

    @Test
    fun searchArtists(){
        val service = SearchService(token)
        service.feachSearchArtists("the", object :
            CallbackData<List<Artist>> {
            override fun onSuccess(value: List<Artist>) {
                System.out.println("Artist!!!!!!")
                for(c : Artist in value){
                    System.out.println(c.name)
                    System.out.println(c.uri)
                    System.out.println(c.id)
                }
            }

            override fun onError(message: String) {
                System.out.println("Error")
            }
        })

        Thread.sleep(3000)
    }

    @Test
    fun searchPlaylist(){
        val service = SearchService(token)
        service.feachSearchPlaylist("the", object :
            CallbackData<List<Playlist>> {
            override fun onSuccess(value: List<Playlist>) {
                System.out.println("Playlist!!!!!!")
                for(c : Playlist in value){
                    System.out.println(c.name)
                    System.out.println(c.uri)
                    System.out.println(c.id)
                    System.out.println(c.owner.name)
                }
            }

            override fun onError(message: String) {
                System.out.println("Error")
            }
        })

        Thread.sleep(3000)
    }

    @Test
    fun searchTrack(){
        val service = SearchService(token)
        service.feachSearchTrack("the", object :
            CallbackData<List<Track>> {
            override fun onSuccess(value: List<Track>) {
                System.out.println("Artist!!!!!!")
                for(c : Track in value){
                    System.out.println(c.name)
                    System.out.println(c.uri)
                    System.out.println(c.album)
                    System.out.println(c.artists[0].name)
                }
            }

            override fun onError(message: String) {
                System.out.println("Error")
            }
        })

        Thread.sleep(3000)
    }

    @Test
    fun addSongToPlaylist(){
        val service = PlaylistService(token)
        var data = ArrayList<Track>()
        var sond = Track.builder().withUri("spotify:track:43zdsphuZLzwA9k4DJhU0I").build()
        var song2 = Track.builder().withUri("spotify:track:1rqqCSm0Qe4I9rUvWncaom").build()
        data.add(sond)
        data.add(song2)

        service.deleteSongsFromPlaylist("5SQjo0p6Cj2hVjMv0RCwrS", data, object :
            CallbackData<String> {
            override fun onSuccess(value: String) {
                System.out.println("aDDED")
            }

            override fun onError(message: String) {
                System.out.println("Error")
            }
        })

        Thread.sleep(6000)
    }

    @Test
    fun deleteSongFromPlaylist(){
        val service = PlaylistService(token)
        var data = ArrayList<Track>()
        var sond = Track.builder().withUri("spotify:track:43zdsphuZLzwA9k4DJhU0I").build()
        var song2 = Track.builder().withUri("spotify:track:1rqqCSm0Qe4I9rUvWncaom").build()
        data.add(sond)
        data.add(song2)
        service.deleteSongsFromPlaylist("5SQjo0p6Cj2hVjMv0RCwrS", data, object :
            CallbackData<String> {
            override fun onSuccess(value: String) {
                System.out.println("Delete")
            }

            override fun onError(message: String) {
                System.out.println("Error")
            }
        })

        Thread.sleep(6000)
    }

    @Test
    fun createNewPlaylist(){
        val fasade = DestinationsFacade(token)
        var data = ArrayList<Track>()
        var sond = Track.builder().withUri("spotify:track:43zdsphuZLzwA9k4DJhU0I").build()
        var song2 = Track.builder().withUri("spotify:track:1rqqCSm0Qe4I9rUvWncaom").build()
        data.add(sond)
        data.add(song2)
        fasade.createNewPlaylist("TestPlaylist", "Nowa playlista",data,  object :
            CallbackData<String> {
            override fun onSuccess(value: String) {
                System.out.println("new playlist " + value)
            }

            override fun onError(message: String) {
                System.out.println("Error " + message )
            }
        })

        Thread.sleep(6000)
    }
}