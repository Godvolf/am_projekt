# SpotMan

An Android native application for efficient management of user's Spotify playlists. Uses Spotify official REST API.

# Technologie do wykorzystania / funkcjonalności do zaimplementowania

* użycie fragmentów;
* obrazki (Picasso) (okładki albumów, playlist, artystów etc.);
* intencje odsyłające do Spotify i/lub odtwarzanie utwórów wewnątrz aplikacji;
* wykorzystanie REST API (Retrofit);
* baza danych (Room);
* wykorzystanie preferencji (Preferences);
* współbieżność (np. AsyncTaski oczekujące na odpowiedzi API);
* RecyclerView (lista utworów);
* wykorzystanie plików (import/export list utworów do pliku).